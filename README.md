## ​​Hippocampal connectivity patterns echo macroscale cortical evolution in the primate brain

Nature Communications (2024). DOI: TBC

Nicole Eichert<sup>1,*</sup>, Jordan DeKraker<sup>2</sup>, Amy F.D. Howard<sup>1</sup>, Istvan N. Huszar<sup>1</sup>, Silei Zhu<sup>1</sup>, Jérôme Sallet<sup>1,3</sup>, Karla L. Miller<sup>1</sup>, Rogier B. Mars<sup>1,4</sup>, Saad Jbabdi<sup>1</sup> & Boris C. Bernhardt<sup>2</sup>
 
<sup>1</sup> Wellcome Centre for Integrative Neuroimaging, Centre for Functional MRI of the Brain (FMRIB), Nuffield Department of Clinical Neurosciences, John Radcliffe Hospital, University of Oxford, Oxford, United Kingdom

<sup>2</sup> Department of Neurology and Neurosurgery, Montreal Neurological Institute and Hospital, McGill University, Canada

<sup>3</sup> INSERM U1208 Stem Cell and Brain Research Institute, Univ Lyon, Bron, France

<sup>4</sup> Donders Institute for Brain, Cognition and Behaviour, Radboud University Nijmegen, Nijmegen, The Netherlands
 
\* corresponding author

## Copyright
University of Oxford. 

## Support
nicole.eichert@ndcn.ox.ac.uk

## Citation
* Paper: Eichert et al., Hippocampal connectivity patterns echo macroscale cortical evolution in the primate brain, Nature Communications, 2024, doi: TBC

* Preprint: Eichert et al., Hippocampal connectivity patterns echo macroscale cortical evolution in the primate brain, bioRxiv, 2023, https://www.biorxiv.org/content/10.1101/2023.09.08.556859v1

* This Gitlab repo: 
Eichert et al. The Hipmac project: analysis scripts, Zenodo 2024,
doi: 10.5281/zenodo.11371630

## License
CC BY-NC-SA 4.0

## Related datasets 
* Hippocampal histology data and subfield annotations: [->Link][1] (Dataset title on the Digital Brain Bank: Hipmac project)
* Screenshots of hippocampal histology data and subfield annotations: [->Link][2]
* The BigMac dataset [->Link][3]  (Dataset title on the Digital Brain Bank: The BigMac Dataset)
* Macaque fMRI data [->Link][4]

## Required Software and Packages
* FSL v6.0: https://fsl.fmrib.ox.ac.uk/fsl/fslwiki
* FSL's TIRL: https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/TIRL
* Freesurfer: https://surfer.nmr.mgh.harvard.edu/
* Hippunfold: https://hippunfold.readthedocs.io/en/latest/
* Brainspace: https://github.com/MICA-MNI/BrainSpace
* ANTs: https://stnava.github.io/ANTs/
* HCP's connectome workbench: https://www.humanconnectome.org/software/connectome-workbench
* Neuroecology lab's MrCAT toolbox: https://github.com/neuroecology/MrCat

[1]: https://open.win.ox.ac.uk/DigitalBrainBank/#/datasets/anatomist
[2]: https://figshare.com/s/252db000141e8d6393af
[3]: https://open.win.ox.ac.uk/DigitalBrainBank/#/datasets/anatomist
[4]: https://osf.io/hke98