%% multiplanar slicing
% original script: Jordan DeKraker 2023
% modified for BigMac: Nicole Eichert 2024
%
%   fsl_sub -q veryshort.q matlab -singleCompThread -nodisplay -nosplash \< $myCode/RS/hipmac/anatomy.m

%  rsync -uvr neichert@jalapeno.fmrib.ox.ac.uk:/vols/Scratch/neichert/hipmac/pngs/old/anatomy.gif /Users/neichert/hipmac/anatomy.gif

% generates slice views along the coronal plane of hippunfold results, and
% along the curved A-P axis
% NOTE this will require hippunfold options: --output_denisty unfoldiso and
% --output_spaces corobl

clear; close all;
warning('off', 'MATLAB:rankDeficientMatrix');
warning('off', 'MATLAB:colon:nonIntegerIndex');

Anat = '/vols/Scratch/neichert/hipmac/BIDS/sub-34/anat/struct_L_cropped_01.nii.gz';
Labelmap = '/vols/Scratch/neichert/hipmac/peewee/acpc/consensus_L_corobl_01.nii.gz';
Midsurf = '/vols/Scratch/neichert/hipmac/output/sub-34/surf/sub-34_hemi-L_space-corobl_den-unfoldiso_label-hipp_midthickness.surf.gii';
Subfields = '/vols/Scratch/neichert/hipmac/hip_surf/consensus_L.func.gii';
outdir = '/vols/Scratch/neichert/hipmac/pngs/old';
filename = 'anatomy';

%% settings for display
subfield_transparency = 0.5;
plane_transparency = 0.25;
linethickness = 5;
fontsize_title = 15;
fontsize_caption = 10;
imsize = 1000; %mm
vertPad = 15; % clipped off at each AP end
size_squares = 0.28;
% timings
nslices = 50;
delay = 0.5;
savefracts = 0.5;
% save every png:
%savefracts = linspace(0,1,nslices);

%% get data
[img, filetype, hdr] = readimgfile(Anat);
lbl = readimgfile(Labelmap);
Vmid = gifti(char(Midsurf));
F = Vmid.faces;
AP_cm = Vmid.vertices(:,2);
Vmid = Vmid.vertices;
subf = readimgfile(Subfields);
colorlims = prctile(img(:),[10,90]); 
consensus = imread('/vols/Scratch/neichert/hipmac/tifs/consensus_L.tif');
consensus = double(consensus(:,:,1));
coords_table = readtable('/vols/Scratch/neichert/hipmac/slices_y_mm.csv', 'Delimiter', ','); 
coords_table = coords_table(strcmp(coords_table.hemi, 'R')==0,:);
ycoords = double(imread('/vols/Scratch/neichert/hipmac/tifs/y_coords_mm_L.tif'));
newMax = 44;
newMin = 27;
ycoords = round((ycoords - min(ycoords(:))) * (newMax - newMin) / (max(ycoords(:)) - min(ycoords(:))) + newMin);

% load default histo slide as initial one 
thisImage = imread('/vols/Scratch/neichert/hipmac/pngs/old/Posterior_CV059a.png');
thisImage = flip(thisImage, 1);

%% my colormap
my_bright_gray = [0.7, 0.7, 0.7];
myorange = [237 125 49]./255;
my_dark_gray = [0.3, 0.3, 0.3];
subfield_colormap = [
    my_dark_gray; %0, line: black
    [50 170 50]./255; %1, CA1: green
    [219 44 45]./255; %2, CA2: red
    [158 114 197]./255; %3, CA3/4: purple
    [23 197 213]./255]; %4, sub:blue
% sub
lbl(lbl==1) = 4;
subf(subf==1) = 4;
consensus(consensus==0) = 4;
% CA 1
lbl(lbl==43) = 1;
subf(subf==43) = 1;
consensus(consensus==42) = 1;
% CA 2
lbl(lbl==86) = 2;
subf(subf==86) = 2;
consensus(consensus==85) = 2;
% CA 3/4
lbl(lbl==128) = 3;
subf(subf==128) = 3;
consensus(consensus==127) = 3;

%% switch vertices back to image space (inverse transform)
res = hdr.qform(1,1);
Vmid(:,1) = (Vmid(:,1)-hdr.qform(1,4)) ./ res;
Vmid(:,2) = (Vmid(:,2)-hdr.qform(2,4)) ./ res;
Vmid(:,3) = (Vmid(:,3)-hdr.qform(3,4)) ./ res;
imsz = imsize*res; %mm to voxels
APres = 254; PDres = 126; IOres = 8;
Vmid(Vmid<0) = 0;
Vxyz = reshape(Vmid,APres,PDres,3);

sz = size(img);
imgInterpolant = griddedInterpolant(single(img));
lblInterpolant = griddedInterpolant(single(lbl),'nearest');
APrange = linspace(vertPad+1,APres-vertPad-1,nslices);
CORrange = linspace(max(Vmid(:,2)),min(Vmid(:,2)),length(APrange));
% 
APrange = flip(APrange);
CORrange = flip(CORrange);

%% main figure
% Set the size of the figure in centimeters
figureWidth = 20;
figureHeight = 20;
h = figure('units', 'centimeters', 'position', [1, 1, figureWidth, figureHeight], 'color', 'w');

% 3D rendering
ax(1) = subplot(4,6,[2 3 8 9]);
title('3D rendering', 'FontSize', fontsize_title, 'Color', 'black')
hold on;
p = patch('Faces',F,'Vertices',Vmid,'FaceVertexCData',subf);
p.LineStyle = 'none';
p.FaceColor = 'interp';
material dull;
axis equal off;
colormap(subfield_colormap);
caxis([0,5])
light;
xlim([1,sz(1)]);
ylim([1,sz(2)]);
zlim([1,sz(3)]);
set(gca,'zdir','reverse');
set(gca,'ydir','reverse');
view([180 -80]);
hold off

% figure caption
str = ['\bfSupplementary File 1. Hippocampal anatomy. ',...
    '\rmHippocampal subfields manually labeled in a reference macaque brain (BigMac) are shown in several displays (left hemisphere only). '...
    '\bfTop Left', '\rm - 3D rendering of the hippocampal subfields. '...
    'A coronal and an oblique plane, orthogonal to the long-axis, are shown as transparent grey planes. '...
    '\bfTop Right', '\rm - The unfolded 2D flatmap of subfields shown with the intersection of the coronal plane and the y-coordinate measured from the back of the brain. '... 
    '\bfBottom Left/Middle', '\rm - The intersection of the MRI volume and subfields with both planes. '...
    '\bfBottom Right', '\rm - The histology slice approximately corresponding to the coronal section. The Dentate Gyrus (yellow) is included to provide orientation only.'];
annotation('textbox',[0, 0, 1, 0.2],'String',str,'FontSize', fontsize_caption, 'EdgeColor','none');

% legend
str = {strcat('\color[rgb]',sprintf('{%d,%d,%d}', subfield_colormap(5,:)),'sub. complex'),...
    strcat('\color[rgb]',sprintf('{%d,%d,%d}', subfield_colormap(2,:)),'CA1'),...
    strcat('\color[rgb]',sprintf('{%d,%d,%d}', subfield_colormap(3,:)),'CA2'),...
    strcat('\color[rgb]',sprintf('{%d,%d,%d}', subfield_colormap(4,:)),'CA3/4')};

annotation('textbox',[0.1, 0.6, 1, 0.2],'String',str,'FontSize', fontsize_caption, 'EdgeColor','none', 'FontWeight', 'bold');

%% initialize loop
s1 = [];
s2 = []; 
n=1;
mytext = annotation('textbox',[0.7, 0.4, 0.1, 0.2],'String', ' ','FontSize', fontsize_caption, 'EdgeColor','none');

for AP = APrange
    disp(n);
    %% compute AP perpendicular
    pts = reshape(Vxyz(AP-vertPad:AP+vertPad,:,:),[(vertPad*2 +1)*PDres,3]); % added points from neighbouring AP slices for smoother movement
    pln = [ones(length(pts),1), pts(:,[1 3])] \ pts(:,2);
    x = [mean(pts(:,1))-imsz : mean(pts(:,1))+imsz];
    y = [mean(pts(:,2))-imsz : mean(pts(:,2))+imsz];
    z = [mean(pts(:,3))-imsz : mean(pts(:,3))+imsz];
    [X,Z] = meshgrid(x,z);
    Y = pln(2)*X(:) + pln(3)*Z(:) + pln(1);
    removex = find(X(:)<1 | X(:)>sz(1));
    removey = find(Y(:)<1 | Y(:)>sz(2));
    removez = find(Z(:)<1 | Z(:)>sz(3));
    remove = unique([removex; removey; removez]);
    X(remove) = 1;
    Y(remove) = 1;
    Z(remove) = 1;
    Xcurve = X;
    Ycurve = Y;
    Zcurve = Z;
    imgSlice_curve = imgInterpolant(X(:),Y(:),Z(:));
    imgSlice_curve = reshape(imgSlice_curve,[length(z) length(x)]);
    imgSlice_curve(remove) = nan;
    lblSlice_curve = lblInterpolant(X(:),Y(:),Z(:));
    lblSlice_curve = reshape(lblSlice_curve,[length(z) length(x)]);
    lblSlice_curve(remove) = 0;
    
    %% compute coronal
    idx = (Vmid(:,2)>(CORrange(n)-2) & Vmid(:,2)<(CORrange(n)+2));
    pts = Vmid(idx,:);
    x = [mean(pts(:,1))-imsz : mean(pts(:,1))+imsz];
    y = [mean(pts(:,2))-imsz : mean(pts(:,2))+imsz];
    z = [mean(pts(:,3))-imsz : mean(pts(:,3))+imsz];
    [X,Z] = meshgrid(x,z);
    Y = repmat(CORrange(n),size(X));
    removex = find(X(:)<1 | X(:)>sz(1));
    removey = find(Y(:)<1 | Y(:)>sz(2));
    removez = find(Z(:)<1 | Z(:)>sz(3));
    remove = unique([removex; removey; removez]);
    X(remove) = 1;
    Y(remove) = 1;
    Z(remove) = 1;
    Xcoronal = X;
    Ycoronal = Y;
    Zcoronal = Z;
    imgSlice_coronal = imgInterpolant(X(:),Y(:),Z(:));
    imgSlice_coronal = reshape(imgSlice_coronal,[length(z) length(x)]);
    imgSlice_coronal(remove) = nan;
    lblSlice_coronal = lblInterpolant(X(:),Y(:),Z(:));
    lblSlice_coronal = reshape(lblSlice_coronal,[length(z) length(x)]);
    lblSlice_coronal(remove) = 0;
    coronal_slice_y_cm = round(mean(AP_cm(idx==1)));
    
    %% plot planes
    subplot(4,6,[2 3 8 9]);
    delete(s1); delete(s2);
    hold on
    s1 = scatter3(Xcurve(:),Ycurve(:),Zcurve(:),1,my_bright_gray,...
        'MarkerEdgeAlpha', plane_transparency, 'MarkerFaceAlpha', plane_transparency);
    s2 = scatter3(Xcoronal(:),Ycoronal(:),Zcoronal(:),1,my_dark_gray,...
        'MarkerEdgeAlpha', plane_transparency, 'MarkerFaceAlpha', plane_transparency);
    hold off
    
    %% plot oblique section
    myax = subplot(4,6,[13 14], 'Position', [0.05, 0.2, size_squares, size_squares]);
    title('oblique section', 'FontSize', fontsize_title, 'Color', my_bright_gray')
    axis off
    f2 = figure(2);
    ax1 = axes;
    hold on
    p1 = imagesc(imgSlice_curve);
    set(ax1,'ydir','normal');
    colormap(ax1,'gray');    
    caxis(ax1,colorlims);
    axis equal tight;
    set(gca,'xdir','reverse');
    
    % subfield overlay
    ax2 = axes;
    p2 = imagesc(lblSlice_curve, 'AlphaData',(lblSlice_curve>0)*subfield_transparency);
    set(ax2,'color','none','visible','off')
    set(ax2,'ydir','normal');
    colormap(ax2,subfield_colormap); 
    caxis([0,5])
    axis equal tight;
    set(gca,'xdir','reverse');
	set(ax1,'xtick',[]);
	set(ax1,'ytick',[]);
    
    % Add a rectangle to represent the bounding box
    rectangle('Position', [0.5, 0.5, size(lblSlice_curve, 2), size(lblSlice_curve, 1)], 'EdgeColor', my_bright_gray, 'LineWidth', linethickness);
    hold off

    % add to main figure
    linkaxes([ax1 ax2]);

    hh = get(f2,'Children');
    newh = copyobj(hh,h);
    
    for j = 1:length(newh)
        possub  = get(myax,'Position');
        set(newh(j),'Position',possub)
    end
    close(f2);
  
    %% plot coronal section
    myax = subplot(4,6,[15 16], 'Position', [0.35, 0.2, size_squares, size_squares]);
    title('coronal section', 'FontSize', fontsize_title, 'Color', my_dark_gray')
    axis off
    f3 = figure(3);
    ax1 = axes;
    hold on
    p1 = imagesc(imgSlice_coronal);
    set(ax1,'ydir','normal');
    colormap(ax1,'gray'); 
    caxis(ax1,colorlims);
    axis equal tight;
    set(gca,'xdir','reverse');
    
    % subfield overlay
    ax2 = axes;
    p2 = imagesc(lblSlice_coronal, 'AlphaData',(lblSlice_coronal>0)*subfield_transparency);
    set(ax2,'color','none','visible','off')
    set(ax2,'ydir','normal');
    colormap(ax2,subfield_colormap);
    caxis([0,5])
    axis equal tight;
    set(gca,'xdir','reverse');    
	set(ax1,'xtick',[]);
	set(ax1,'ytick',[]);

    % Add a rectangle to represent the bounding box
    rectangle('Position', [0.5, 0.5, size(lblSlice_coronal, 2), size(lblSlice_coronal, 1)], 'EdgeColor', my_dark_gray, 'LineWidth', linethickness);
    hold off

    % add to main figure
    hh = get(f3,'Children');
    newh = copyobj(hh,h);
    linkaxes([ax1 ax2]);
    set(gca,'ydir','reverse');
    
    for j = 1:length(newh)
        possub  = get(myax,'Position');
        set(newh(j),'Position',possub)
    end
    close(f3);
    
    %% plot histo slice
    [m,slice_idx] = min(abs(coords_table.y_mm - coronal_slice_y_cm));
    png_name = char(strcat(coords_table{slice_idx,'blockname'}, '_', coords_table{slice_idx,'slicename'}));
    png_name = strrep(png_name, '_ST_150', '');
    png_name = strrep(png_name, '_bottom', '');
    % assume screenshot taken from left size of x-slide
    png_name = strrep(png_name, 'x', 'a');
    histo_fname = strcat('/vols/Scratch/neichert/hipmac/pngs/old/', png_name, '.png');
    if exist(char(histo_fname), 'file')
         thisImage = imread(histo_fname);
         %thisImage = flip(thisImage, 1);
    else
         disp(histo_fname)
    end
    ax1 = subplot(4,6,[17 18], 'Position', [0.65, 0.2, size_squares, size_squares]);
    hold on
    imshow(thisImage);
    axis equal tight;
    set(ax1, 'xtick', []);
    set(ax1, 'ytick', []);
    % Add a rectangle to represent the bounding box
    rectangle('Position', [0.5, 0.5, size(thisImage, 2), size(thisImage, 1)], 'EdgeColor', 'black', 'LineWidth', linethickness);
    hold off
    title('histology', 'FontSize', fontsize_title, 'Color', 'black')
    
    %% add flatmap    
    ax1 = subplot(4,6,[4 5 10 11]);
    title('2D flatmap', 'FontSize', fontsize_title, 'Color', my_dark_gray')
    hold on
    consensus_with_line = consensus;
    consensus_with_line(ycoords==coronal_slice_y_cm)=0;
    imagesc(consensus_with_line);
    colormap(ax1,subfield_colormap);
    caxis([0,5])
    axis image
    set(ax1,'xtick',[]);
	set(ax1,'ytick',[]);
    hold off
    set(gca,'ydir','reverse');
    
    %% y-coord textbox
    y_from_back_of_brain = num2str(round((coronal_slice_y_cm - 2.65)/10, 2));
    delete(mytext)
    mytext = annotation('textbox',[0.75, 0.37, 0.1, 0.2],'String', strcat('y=', y_from_back_of_brain, 'cm'),'FontSize', fontsize_caption, 'EdgeColor','none');
   
    %% Save the plot as an image
    output = [outdir, '/', filename '.gif'];
    frame = getframe(h);
    im = frame2im(frame);
    [imind,cm] = rgb2ind(im,256);
    if n == 1
        imwrite(imind,cm, output,'gif','DelayTime',delay, 'Loopcount',inf);
    else
        imwrite(imind,cm, output,'gif','DelayTime',delay, 'WriteMode','append');
    end
    
    % capture a few still frames
    i = ismember(savefracts*nslices,n-1);
    if any(i)
        fpath = strcat(outdir, '/', filename, int2str(n), '.png');
        imwrite(imind,cm,fpath);
        disp(fpath);
    end
    n=n+1;
end
disp(output);
close all;