import java.lang.reflect.Type;
def gson = GsonTools.getInstance(true)
def imageData = QPEx.getCurrentImageData()
def server = imageData.getServer()
import java.nio.file.Paths

// output directory
def rootdir = '/home/fs0/neichert/scratch/hipmac/annotations/'

// make new directory for this slice
String path = server.getPath()
ppath = path.split('/')


def values = ppath[-1].split('_')
def hemi = values[2].toString().split('.tif')
fname = rootdir + '/Cresyl/' + values[0] + '/' + values[1] + 'x/structureTensor' + '/' + values[1] + hemi[0] + '_ST_150/annotations_cropped.geojson'


print fname

def myjson = new File(fname).text

// Read the annotations
def type = new com.google.gson.reflect.TypeToken<List<qupath.lib.objects.PathObject>>() {}.getType()

def deserializedAnnotations = gson.fromJson(myjson, type)
addObjects(deserializedAnnotations)  