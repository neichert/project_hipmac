## Figure 1A
for hemi in L R; do
    vol=/vols/Scratch/neichert/hipmac/sub-40_x_vol_${hemi}.nii.gz
    #mysurf=/vols/Scratch/neichert/hipmac/output/sub-40/surf/sub-40_hemi-${hemi}_space-corobl_den-0p5mm_label-hipp_midthickness.surf.gii
    mysurf=/vols/Scratch/neichert/hipmac/output/sub-40/surf/sub-40_hemi-${hemi}_space-corobl_den-unfoldiso_label-hipp_midthickness.surf.gii
    outfunc=/vols/Scratch/neichert/hipmac/sub-40_x_vol_${hemi}.func.gii
    wb_command -volume-to-surface-mapping ${vol} ${mysurf} ${outfunc} -enclosing
done

wb_command -cifti-create-dense-scalar /vols/Scratch/neichert/hipmac/sub-40_x_vol.dscalar.nii \
        -left-metric /vols/Scratch/neichert/hipmac/sub-40_x_vol_L.func.gii \
        -right-metric /vols/Scratch/neichert/hipmac/sub-40_x_vol_R.func.gii

wb_command -cifti-math '(ceil(x))' /vols/Scratch/neichert/hipmac/sub-40_x_vol.dscalar.nii  -var "x"  /vols/Scratch/neichert/hipmac/sub-40_x_vol.dscalar.nii


## Registration BigMac <> CIVM
SD=/vols/Data/sj/Nicole/postmort
subj='peewee'
BIDSdir=/vols/Scratch/neichert/hipmac/BIDS

fixed=/vols/Data/sj/Nicole/postmort/CIVM_template/civm_rhesus_v1_gre_03.nii.gz
fixed_mask=/vols/Data/sj/Nicole/postmort/CIVM_template/civm_rhesus_v1_gre_03_brain_mask.nii.gz
fixed_01=/vols/Data/sj/Nicole/postmort/CIVM_template/civm_rhesus_v1_gre.nii.gz
moving=$SD/${subj}/anat/struct_fullfov.nii.gz
moving_mask=$SD/${subj}/anat/struct_fullfov_mask.nii.gz
logdir=$SD/logs

# derive registration
fsl_sub -l $logdir $ANTSPATH/antsRegistrationSyNQuick.sh -d 3 -f $fixed -m $moving -o $SD/$subj/pmtempl/${subj}_civet_ -x $fixed_mask

to_warp = ''

padding=5
for hemi in L R; do
    echo $hemi
    to_warp=$BIDSdir/sub-34/anat/sub-34_hemi-${hemi}_dseg.nii.gz
    warped_03=$BIDSdir/sub-40/anat/hip_${hemi}_dseg_from_peewee.nii.gz
    tmp=$BIDSdir/sub-40/anat/tmp.nii.gz

    dims=`fslstats $warped_03 -w`
    dims=($dims)
    xmin=$((${dims[0]} - $padding))
    xsize=$((${dims[1]} + $padding + $padding))
    ymin=$((${dims[2]} - $padding))
    ysize=$((${dims[3]} + $padding + $padding))
    zmin=$((${dims[4]} - $padding))
    zsize=$((${dims[5]} + $padding + $padding))

    echo 'crop anat 03'
    fslroi $fixed \
    $BIDSdir/sub-40/anat/struct_${hemi}_cropped_03.nii.gz \
    $xmin $xsize $ymin $ysize $zmin $zsize 0 1


    applywarp -i $BIDSdir/sub-40/anat/struct_${hemi}_cropped_03.nii.gz -o $tmp -r $BIDSdir/sub-40/test.nii.gz --usesqform
    applywarp -i $tmp -o $tmp -r $fixed_01 --usesqform

    echo 'crop anat 01'
    fslroi $fixed_01 $BIDSdir/sub-40/anat/struct_${hemi}_cropped_01.nii.gz $(fslstats $tmp -w)

    echo 'upsample segmentation'
    $ANTSPATH/antsApplyTransforms -i $warped_03 \
    -o $BIDSdir/sub-40/anat/hip_${hemi}_dseg_from_peewee_01.nii.gz \
    --interpolation MultiLabel -r $BIDSdir/sub-40/anat/struct_${hemi}_cropped_01.nii.gz

    # then manually edit the highres
    # then run hippunfold on sub-40
done


## CIVM hippocampal surface to Yerkes
DD=/vols/Data/sj/Nicole/postmort/CIVM_template
moving=$DD/civm_rhesus_v1_gre_03.nii.gz
moving_mask=$DD/civm_rhesus_v1_gre_03_brain_mask.nii.gz
fixed=/vols/Scratch/neichert/NHPPipelines/global/templates/MacaqueYerkes19_T2w_0.5mm_dedrift_brain.nii.gz
fixed_mask=/vols/Scratch/neichert/NHPPipelines/global/templates/MacaqueYerkes19_T1w_0.5mm_brain_mask_dedrift.nii.gz

logdir=/vols/Data/sj/Nicole/postmort/CIVM_template/
fsl_sub -l $logdir \
$ANTSPATH/antsRegistrationSyNQuick.sh -d 3 -f $fixed -m $moving \
-o $DD/civm2yerkes_ -x $fixed_mask


wbdir=/vols/Scratch/neichert/workbench/bin_rh_linux64

# convert ants to world
fsl_sub -q short.q $wbdir/wb_command -convert-warpfield \
-from-itk $DD/civm2yerkes_1Warp.nii.gz \
-to-world $DD/civm2yerkes_1Warp_world.nii.gz

fsl_sub -q short.q $wbdir/wb_command -convert-warpfield \
-from-itk $DD/civm2yerkes_1InverseWarp.nii.gz \
-to-world $DD/civm2yerkes_1InverseWarp_world.nii.gz

# apply warpfield
$c3dbin/c3d_affine_tool \
-ref $fixed \
-src $moving  \
-itk $DD/civm2yerkes_0GenericAffine.mat \
-ras2fsl -o $DD/civm2yerkes_0GenericAffine_flirt.mat

# warp surface
DD=/vols/Data/sj/Nicole/postmort/CIVM_template/Yerkes
wbdir=/vols/Scratch/neichert/workbench/bin_rh_linux64
moving=/vols/Data/sj/Nicole/postmort/CIVM_template/civm_rhesus_v1_gre_03.nii.gz
fixed=/vols/Scratch/neichert/NHPPipelines/global/templates/MacaqueYerkes19_T2w_0.5mm_dedrift_brain.nii.gz
#density_list='0p5mm 2mm'
density_list='unfoldiso'
for density in $density_list; do
    for hemi in L R; do
        for surf in inner midthickness outer; do
            fname_out=$DD/${hemi}_${density}_${surf}.surf.gii
            surfin=/vols/Scratch/neichert/hipmac/output/sub-40/surf/sub-40_hemi-${hemi}_space-corobl_den-${density}_label-hipp_${surf}.surf.gii
            # first linear
            $wbdir/wb_command -surface-apply-affine $surfin \
            $DD/../civm2yerkes_0GenericAffine_flirt.mat \
            $DD/test.surf.gii \
            -flirt $moving $fixed

            # then nonlinear
            $wbdir/wb_command -surface-apply-warpfield $DD/test.surf.gii \
            $DD/../civm2yerkes_1InverseWarp_world.nii.gz \
            $fname_out
            echo $fname_out
        done
    done
done


# other snippets for structural processing

if [[ $OSTYPE == "linux" ]] ; then
  myscratch=/vols/Scratch/neichert
elif [[ $OSTYPE == "darwin" ]] ; then
  myscratch=/Users/neichert
  ANTSPATH='/Users/neichert/code/install/bin'
elif [[ $OSTYPE == "linux-gnu" ]] ; then
  myscratch='/data/mica1/03_projects/neichert/'
  ANTSPATH='/opt/minc-itk4/bin'
fi


# start from raw image
cp /Users/neichert/hipmac/valhalla/valhalla_mge3d_sqrtsos001.nii.gz /Users/neichert/hipmac/valhalla/struct.nii.gz
img=/Users/neichert/hipmac/valhalla/anat/struct.nii.gz

# fix labels and axes
sh $MRCATDIR/core/swapdims.sh $img -z -x -y $img 'image'

# run structural processing
cd /Users/neichert/hipmac
sh $MRCATDIR/core/struct_macaque.sh --subjdir=valhalla --structImg=$img --structdir=valhalla/anat \
--all

sh $MRCATDIR/core/struct_macaque.sh --subjdir=valhalla --structImg=$img --structdir=valhalla/anat

# gibbs ringing
img=/Users/neichert/hipmac/valhalla/anat/struct_restore_brain.nii.gz
imgout=/Users/neichert/hipmac/valhalla/anat/struct_restore_brain_gibbs.nii.gz
mrdegibbs $img $imgout

# templates
# get 0.3 F99 version
flirt -dof 6 -cost corratio -nosearch -in /Users/neichert/code/MrCat-dev/data/macaque/F99/McLaren \
-ref /Users/neichert/code/MrCat-dev/data/macaque/F99/McLaren \
-omat /Users/neichert/hipmac/valhalla/transform/F99_5_to_3 \
-applyisoxfm 0.3 -out /Users/neichert/hipmac/stuff/valhalla/F99/McLaren_03

# get Yerkes template
flirt -dof 6 -cost corratio -nosearch \
-in /vols/Scratch/neichert/NHPPipelines/global/templates/MacaqueYerkes19_T1w_0.5mm_dedrift_brain.nii.gz \
-ref /vols/Scratch/neichert/NHPPipelines/global/templates/MacaqueYerkes19_T1w_0.5mm_dedrift_brain.nii.gz \
-applyisoxfm 0.3 -out /vols/Scratch/neichert/templating/ref_brain.nii.gz

fslroi ref_brain.nii.gz ref_brain2.nii.gz 63 196 60 242 80 166

# lin
flirt -dof 6 -cost corratio -nosearch -noresample -in /Users/neichert/hipmac/valhalla/anat/struct_restore_brain \
-ref /Users/neichert/hipmac/valhalla/acpc/struct_brain_restore_gibbs.nii.gz \
-omat /Users/neichert/hipmac/valhalla/transform/struct_to_acpc_03.mat \
-out /Users/neichert/hipmac/valhalla/acpc/struct_restore_lin_03

# downsample highres dseg from 0.1 to 0.3
flirt -in /vols/Scratch/neichert/hipmac/valhalla/acpc/sub-valhalla_hemi-L_0p1mm_dseg.nii.gz \
 -ref /vols/Scratch/neichert/hipmac/valhalla/acpc/struct_restore_bseg.nii.gz \
  -applyxfm -usesqform \
  -out /vols/Scratch/neichert/hipmac/valhalla/acpc/sub-valhalla_hemi-L_0p3mm_dseg.nii.gz \
   -interp nearestneighbour

# applywarp
applywarp -i /Users/neichert/hipmac/stuff/valhalla/anat/struct_restore_bseg.nii.gz \
 -o /Users/neichert/hipmac/valhalla/acpc/struct_restore_bseg.nii.gz \
 -r /Users/neichert/hipmac/valhalla/F99/McLaren_03 \
 --premat=/Users/neichert/hipmac/valhalla/transform/struct_to_acpc_03.mat --interp=nn

# applywarp
applywarp -i /vols/Scratch/neichert/hipmac/valhalla/acpc/sub-valhalla_hemi-L_0p3mm_dseg.nii.gz \
 -o /vols/Scratch/neichert/hipmac/valhalla/F99/sub-valhalla_hemi-L_0p3mm_dseg.nii.gz \
 -r /vols/Scratch/neichert/hipmac/valhalla/F99/McLaren_03 \
 -w /vols/Scratch/neichert/hipmac/valhalla/transform/acpc2F99_warp.nii.gz --interp=nn

# valhalla to Yerkes
flirt -in $myscratch/hipmac/valhalla/acpc/struct_brain_restore_gibbs \
-ref $myscratch/hipmac/peewee/Yerkes/struct_brain.nii.gz \
-omat $myscratch/hipmac/valhalla/transform/acpc2Yerkes.mat \
-o $myscratch/hipmac/valhalla/Yerkes/struct_brain_lin.nii.gz

fsl_sub -q bigmem.q fnirt --in=$myscratch/hipmac/valhalla/acpc/struct_brain_restore_gibbs \
--ref=$myscratch/hipmac/peewee/Yerkes/struct_brain.nii.gz \
--aff=$myscratch/hipmac/valhalla/transform/acpc2Yerkes.mat \
--iout=$myscratch/hipmac/valhalla/Yerkes/struct_brain.nii.gz \
--fout=$myscratch/hipmac/valhalla/transform/acpc2Yerkes \
--cout=$myscratch/hipmac/valhalla/transform/acpc2Yerkes_warpcoef \
--config=/vols/Scratch/saad/atlases/F99_atlas/config


# ants templating
cd /vols/Data/sj/Nicole/postmort
cp /vols/Data/km/ahoward/BigMac-clean/MRI/Postmortem/struct/MGE/data/struct_brain.nii.gz peewee/struct_brain.nii.gz
cp /vols/Scratch/neichert/hipmac/ultimate/acpc/struct_restore_brain.nii.gz ultimate/struct_brain.nii.gz
cp /vols/Scratch/neichert/hipmac/pugsley/acpc/struct_restore_brain.nii.gz pugsley/struct_brain.nii.gz
cp /vols/Scratch/neichert/hipmac/valhalla/Yerkes/struct_brain_lin.nii.gz valhalla/struct_brain.nii.gz

cp /vols/Data/postmort/RBM/macaque_BSB_pat/raw/70_20180413_Extra.nifti/NS1_AK5_G_3327_70_mge3d_SOS_044.nifti/MG/sqrtsos001.nii \
pat/struct_brain.nii
mri_convert pat/struct_brain.nii pat/struct_brain.nii.gz
rm pat/struct_brain.nii
img=pat/struct_brain.nii.gz
sh $MRCATDIR/core/swapdims.sh $img -z -x -y $img 'image'

cp /vols/Data/postmort/RBM/macaque_BSB_pleco/raw/fr.30678.0.70_20180831_Extra.nifti/NS1_AK5_G_3327_70_mge3d_057.nifti/MG/sqrtsos001.nii \
pleco/struct_brain.nii
mri_convert pleco/struct_brain.nii pleco/struct_brain.nii.gz
rm pleco/struct_brain.nii
img=pleco/struct_brain.nii.gz
sh $MRCATDIR/core/swapdims.sh $img z -x y $img 'image'

cp /vols/Data/postmort/RBM/macaque_BSB_travis/raw/70_20200313_SEMSDW.nifti/NS1_AK5_G_5340_70_mge3d_038_SOS.nifti/MG/sqrtsos001.nii \
travis/struct_brain.nii
mri_convert travis/struct_brain.nii travis/struct_brain.nii.gz
rm travis/struct_brain.nii
img=travis/struct_brain.nii.gz
sh $MRCATDIR/core/swapdims.sh $img z x y $img 'image'

# valhalla to Yerkes
subj_list='peewee ultimate pugsley valhalla pat pleco travis'
flirt -in $myscratch/hipmac/valhalla/acpc/struct_brain_restore_gibbs \
-ref $myscratch/hipmac/peewee/Yerkes/struct_brain.nii.gz \
-omat $myscratch/hipmac/valhalla/transform/acpc2Yerkes.mat \
-o $myscratch/hipmac/valhalla/Yerkes/struct_brain_lin.nii.gz

# Docker
docker pull khanlab/hippunfold:latest

docker run -it --rm \
khanlab/hippunfold:latest \
-h

PATH_TO_BIDS_DIR=/Users/neichert/hipmac/bids
PATH_TO_OUTPUT_DIR=/Users/neichert/hipmac/output
participant='sub-01'

docker run -it --rm \
-v bids:/bids:ro \
-v test:/output \
khanlab/hippunfold:latest \
./bids ./output participant --modality T1w --output-density unfoldiso 0p5mm 2mm  -p --cores all


docker run -it --rm \
-v /Users/neichert/hipmac/bids:/bids:ro \
-v /Users/neichert/hipmac/output:/output \
khanlab/hippunfold:latest \
./bids ./output participant --modality T1w -p --cores all


docker run -it --rm \
-v /Users/neichert/hipmac/bids:/bids:ro \
-v /Users/neichert/hipmac/output:/output \
khanlab/hippunfold:latest \
./bids ./output participant --modality T1w \
--participant_label mni \
-p --cores all

surf=/vols/Scratch/neichert/LarynxRepresentation/derivatives/sub-01/T1w/Native/sub-01.L.midthickness.native.anat.surf.gii
warp=/vols/Scratch/neichert/LarynxRepresentation/derivatives/sub-01/anat/anat2MNI24.nii.gz
warpinv=/vols/Scratch/neichert/LarynxRepresentation/derivatives/sub-01/anat/MNI24_2anat.nii.gz
out=/vols/Scratch/neichert/hipmac/test.surf.gii
#out=/vols/Scratch/neichert/hipmac/MNI_L_midthickness.surf.gii
wb_command -surface-apply-warpfield $surf $warpinv $out \
-fnirt $warp


# upsample pugsley
flirt -dof 6 -cost corratio -nosearch -in /Users/neichert/hipmac/pugsley/acpc/sub-pugsley_hemi-L_desc-hippo_mge.nii.gz \
 -ref /Users/neichert/hipmac/pugsley/acpc/sub-pugsley_hemi-L_desc-hippo_mge.nii.gz -omat \
 /Users/neichert/hipmac/pugsley/transform/struct03_2_01.mat -applyisoxfm 0.1 \
 -out /Users/neichert/hipmac/pugsley/acpc/sub-pugsley_hemi-L_desc01mm-hippo_mge.nii.gz

input=$myscratch/hipmac/pugsley/acpc/sub-pugsley_hemi-L_desc-hippo_dseg.nii.gz
premat=$myscratch/hipmac/pugsley/transform/struct03_2_01.mat
output=$myscratch/hipmac/pugsley/acpc/sub-pugsley_hemi-L_desc01mm-hippo_dseg.nii.gz
ref=$myscratch/hipmac/pugsley/acpc/sub-pugsley_hemi-L_desc01mm-hippo_mge.nii.gz
#applywarp -i $input -o $output --premat=$premat -r $ref --interp=nn
antsApplyTransforms -i $input -o $output --interpolation MultiLabel -r $ref


# Docker pugsley
PATH_TO_BIDS_DIR=/Users/neichert/hipmac/bids
PATH_TO_OUTPUT_DIR=/Users/neichert/hipmac/output
participant='sub-pugsley'

docker run -it --rm \
-v /Users/neichert/hipmac/bids:/bids:ro \
-v /Users/neichert/hipmac/output:/output \
khanlab/hippunfold:latest \
./bids ./output participant --participant-label pugsley --modality cropseg \
--path_cropseg ./bids/sub-{subject}/anat/sub-{subject}_hemi-{hemi}_desc01mm-hippo_dseg.nii.gz \
--hemi L --skip_inject_template_labels --cores all

cd /home/bic/neichert/hipmac
singularity run -B /data:/data -B /home:/home \
--env _JAVA_OPTIONS='-Xms512m -Xmx16g' /data/mica3/opt/singularity/hippunfold_v1.0.0.sif \
. ./output participant  \
--participant-label pugsley --modality cropseg \
--path_cropseg /data/mica3/BigMac/sub-02/sub-{subject}_hemi-{hemi}_desc01mm-hippoJD_dseg.nii.gz \
--hemi L --skip_inject_template_labels --cores 12

#try again, this works
cd /data/mica1/03_projects/neichert/hipmac/
singularity run -B /data:/data -B /home:/home -B /host:/host \
--env _JAVA_OPTIONS='-Xms512m -Xmx64g' /data/mica3/opt/singularity/hippunfold_v1.2.0.sif \
/data/mica1/03_projects/neichert/hipmac/BIDS output participant --modality cropseg \
--hemi R --output-density unfoldiso 0p5mm --skip_inject_template_labels \
--participant_label 32 \
--keep-work --keep-going --cores 12


# downsample highres dseg from 0.1 to 0.3
flirt -dof 6 -cost corratio -nosearch -in /vols/Scratch/neichert/hipmac/pugsley/acpc/sub-pugsley_hemi-L_desc01mm-hippoJD_dseg.nii.gz \
 -ref /vols/Scratch/neichert/hipmac/pugsley/acpc/hemi_L_dseg.nii.gz \
 -omat /vols/Scratch/neichert/hipmac/pugsley/transform/acpc01_to_03.mat -applyisoxfm 0.3 \
 -out /vols/Scratch/neichert/hipmac/pugsley/acpc/sub-pugsley_hemi-L_desc03mm-hippoJD_dseg.nii.gz -interp nearestneighbour

# applywarp
applywarp -i /vols/Scratch/neichert/hipmac/pugsley/acpc/sub-pugsley_hemi-L_desc03mm-hippoJD_dseg.nii.gz \
 -o /vols/Scratch/neichert/hipmac/pugsley/F99/sub-pugsley_hemi-L_desc03mm-hippoJD_dseg.nii.gz \
 -r /vols/Scratch/neichert/hipmac/valhalla/F99/McLaren_03 \
 -w /vols/Scratch/neichert/hipmac/pugsley/transform/struct_to_F99_warp.nii.gz --interp=nn

## other stuff
$FSLDIR/data/standard/MNI152_T1_1mm_brain.nii.gz

#/vols/Data/sj/Nicole/site-ucdavis/derivatives/group/rest/test_h.nii.gz
input=/vols/Scratch/neichert/hipmac/sub-01_ref.nii.gz
ref=$FSLDIR/data/standard/MNI152_T1_1mm_brain.nii.gz

# lin
fsl_sub flirt -dof 12 -cost corratio \
-in $input \
-ref $ref \
-omat /vols/Scratch/neichert/hipmac/raw2mni.mat \
-out /vols/Scratch/neichert/hipmac/test.nii.gz

fsl_sub -j flirt fnirt --in=$input \
--ref=$ref \
--aff=/vols/Scratch/neichert/hipmac/raw2mni.mat \
--iout=/vols/Scratch/neichert/hipmac/test.nii.gz \
--fout=/vols/Scratch/neichert/hipmac/raw2mni_warp

fsl_sub -j fnirt invwarp -w /vols/Scratch/neichert/hipmac/raw2mni_warp.nii.gz -r $input -o /vols/Scratch/neichert/hipmac/mni2raw_warp.nii.gz


# get warp
outwarp=/vols/Scratch/neichert/LarynxRepresentation/derivatives/sub-01/anat/anat2MNI1mm.nii.gz
ref=$FSLDIR/data/standard/MNI152_T1_1mm_brain.nii.gz
premat=/vols/Scratch/neichert/LarynxRepresentation/derivatives/sub-01/anat/anat_1mm_to_acpc_0.7mm.mat
warp1=/home/fs0/neichert/scratch/LarynxRepresentation/derivatives/sub-01/MNINonLinear/xfms/acpc_dc2standard.nii.gz
convertwarp -o $outwarp -r $ref --premat=$premat --warp1=$warp1

# apply
input=/vols/Scratch/neichert/hipmac/output/hippunfold/sub-01/anat/sub-01_hemi-L_space-T1w_desc-subfields_atlas-bigbrain_dseg.nii.gz
warp=/vols/Scratch/neichert/LarynxRepresentation/derivatives/sub-01/anat/anat2MNI1mm.nii.gz
output=/vols/Scratch/neichert/hipmac/human_dseg.nii.gz
ref=$FSLDIR/data/standard/MNI152_T1_1mm_brain.nii.gz
applywarp -i $input -o $output -w $warp -r $ref --interp=nn

anat=$myscratch/LarynxRepresentation/derivatives/sub-01/anat/sub-01_T1w.nii.gz
invwarp -w $warp -r $anat -o $myscratch/LarynxRepresentation/derivatives/sub-01/anat/MNI24_2anat.nii.gz

# apply inv
input=$FSLDIR/data/standard/MNI152_T1_1mm_brain.nii.gz
warp=$myscratch/LarynxRepresentation/derivatives/sub-01/anat/MNI24_2anat.nii.gz
output=/vols/Scratch/neichert/test.nii.gz
ref=$myscratch/LarynxRepresentation/derivatives/sub-01/anat/sub-01_T1w.nii.gz
applywarp -i $input -o $output -w $warp -r $ref

# surf to MNI
surf=$myscratch/hipmac/output/hippunfold/sub-01/surf/sub-01_hemi-L_space-T1w_den-0p5mm_label-hipp_midthickness.surf.gii
warp=/vols/Scratch/neichert/LarynxRepresentation/derivatives/sub-01/anat/anat2MNI1mm.nii.gz
warpinv=$myscratch/LarynxRepresentation/derivatives/sub-01/anat/MNI24_2anat.nii.gz
out=$myscratch/hipmac/human_L_hip_midthickness.surf.gii
wb_command -surface-apply-warpfield $surf $warpinv $out -fnirt $warp


## pugsley
raw=/vols/Data/postmort/RBM/macaque_BSB_pugsley/raw/fr.28934.0.70_20180629_Extra.nifti/NS1_AK5_G_3327_70_ssfp3d_v4_045.nifti/MG/sqrtsos001.nii
DD=$myscratch/hipmac/pugsley
mri_convert $raw $DD/anat/raw.nii.gz

# fix labels and axes

sh $MRCATDIR/core/swapdims.sh $DD/anat/raw.nii.gz -z -x y $DD/anat/raw.nii.gz 'image'

# anat to acpc registration
flirt -dof 6 -cost corratio -nosearch -in $DD/anat/raw.nii.gz \
-ref $myscratch/hipmac/valhalla/F99/McLaren_03.nii.gz \
-omat $DD/transform/anat2acpc.mat

applywarp -i $DD/anat/raw.nii.gz -r $myscratch/hipmac/valhalla/F99/McLaren_03.nii.gz -o $DD/acpc/raw.nii.gz --premat=$DD/transform/anat2acpc.mat

# gibbs ringing
#conda activate mrtrix3
mrdegibbs $DD/acpc/raw.nii.gz $DD/acpc/struct.nii.gz


# run structural processing
cd $myscratch/hipmac
fsl_sub sh $MRCATDIR/core/struct_macaque.sh --subjdir=pugsley --structImg=$DD/acpc/struct.nii.gz --structdir=pugsley/acpc \
--all

# get dseg in struct space
input=$myscratch/hipmac/valhalla/F99/hemi-L_dseg_nonlin.nii.gz
ref=$DD/acpc/struct_restore_brain.nii.gz
applywarp -i $input -o $DD/acpc/hemi_L_dseg.nii.gz -r $ref \
-w $DD/transform/F99_to_struct_warp.nii.gz --interp=nn


# pugsley to peewee

# lin
flirt -dof 12 -cost corratio \
-in /vols/Scratch/neichert/hipmac/pugsley/acpc/struct_restore_brain.nii.gz \
-ref /vols/Data/km/ahoward/BigMac-clean/MRI/Postmortem/struct/MGE/data/struct_brain.nii.gz \
-omat /vols/Scratch/neichert/hipmac/pugsley/transform/acpc_2_peepwee.mat \
-out /vols/Scratch/neichert/hipmac/peewee/acpc/test.nii.gz

fnirt --in=/vols/Scratch/neichert/hipmac/pugsley/acpc/struct_restore_brain.nii.gz \
--ref=/vols/Data/km/ahoward/BigMac-clean/MRI/Postmortem/struct/MGE/data/struct_brain.nii.gz \
--aff=/vols/Scratch/neichert/hipmac/pugsley/transform/acpc_2_peepwee.mat \
--iout=/vols/Scratch/neichert/hipmac/peewee/acpc/test.nii.gz \
--fout=/vols/Scratch/neichert/hipmac/pugsley/transform/acpc_to_peewee_warp  \
--config=/vols/Scratch/saad/atlases/F99_atlas/config


invwarp -w /vols/Scratch/neichert/hipmac/pugsley/transform/acpc_to_peewee_warp \
 -r /vols/Scratch/neichert/hipmac/pugsley/acpc/struct_restore_brain.nii.gz \
 -o /vols/Scratch/neichert/hipmac/pugsley/transform/peewee_to_acpc_warp

# get pugsley to peewee for warp to work

applywarp -i /vols/Scratch/neichert/hipmac/pugsley/acpc/sub-pugsley_hemi-L_desc01mm-hippoJD_dseg.nii.gz \
 -o /vols/Scratch/neichert/hipmac/peewee/acpc/sub-pugsley_hemi-L_desc01mm-hippoJD_dseg.nii.gz \
 -r /vols/Data/km/ahoward/BigMac-clean/MRI/Postmortem/struct/MGE/data/struct_brain.nii.gz \
 -w /vols/Scratch/neichert/hipmac/pugsley/transform/acpc_to_peewee_warp --interp=nn


# downsample for warp to work
flirt -in /vols/Scratch/neichert/hipmac/output/sub-02/hippunfold/sub-pugsley/anat/sub-pugsley_hemi-L_space-corobl_desc-subfields_atlas-bigbrain_dseg.nii.gz \
 -ref /vols/Scratch/neichert/hipmac/pugsley/acpc/struct_restore_brain.nii.gz \
  -applyxfm -usesqform \
  -out /vols/Scratch/neichert/hipmac/output/sub-02/hippunfold/sub-pugsley/anat/sub-pugsley_hemi-L_space-corobl_desc-subfields_atlas-bigbrain03_dseg.nii.gz \
   -interp nearestneighbour

applywarp -i /vols/Scratch/neichert/hipmac/output/sub-02/hippunfold/sub-pugsley/anat/sub-pugsley_hemi-L_space-corobl_desc-subfields_atlas-bigbrain03_dseg.nii.gz \
 -o /vols/Scratch/neichert/hipmac/peewee/acpc/sub-pugsley_hemi-L_bigbrain_dseg.nii.gz \
 -r /vols/Data/km/ahoward/BigMac-clean/MRI/Postmortem/struct/MGE/data/struct_brain.nii.gz \
 -w /vols/Scratch/neichert/hipmac/pugsley/transform/acpc_to_peewee_warp --interp=nn

# puglsey surf to peewee
# surf to MNI
surf=/home/fs0/neichert/scratch/hipmac/output/sub-02/hippunfold/sub-pugsley/surf/sub-pugsley_hemi-L_space-corobl_den-0p5mm_label-hipp_midthickness.surf.gii
warp=/vols/Scratch/neichert/hipmac/pugsley/transform/acpc_to_peewee_warp.nii.gz
warpinv=/vols/Scratch/neichert/hipmac/pugsley/transform/peewee_to_acpc_warp.nii.gz
out=~/scratch/hipmac/peewee/acpc/pugsley_L_hip_midthickness.surf.gii
wb_command -surface-apply-warpfield $surf $warpinv $out -fnirt $warp

surf=~/scratch/hipmac/peewee/acpc/pugsley_L_hip_midthickness.surf.gii
mat=/vols/Scratch/neichert/hipmac/nudge.mat
out=~/scratch/hipmac/peewee/acpc/pugsley_L_hip_midthickness_box.surf.gii
source=/vols/Data/km/ahoward/BigMac-clean/MRI/Postmortem/struct/MGE/data/struct_brain.nii.gz
target=/vols/Scratch/neichert/hipmac/tmp.nii.gz
wb_command -surface-apply-affine $surf $mat $out -flirt $source $target


# pugsley to Yerkes

 flirt -in $myscratch/hipmac/pugsley/acpc/struct_restore_brain \
 -ref $myscratch/hipmac/peewee/Yerkes/struct_brain.nii.gz \
 -omat $myscratch/hipmac/pugsley/transform/acpc2Yerkes.mat \
 -o $myscratch/hipmac/pugsley/Yerkes/struct_brain_lin.nii.gz

 fnirt --in=$myscratch/hipmac/pugsley/acpc/struct_restore_brain \
 --ref=$myscratch/hipmac/peewee/Yerkes/struct_brain.nii.gz \
 --aff=$myscratch/hipmac/pugsley/transform/acpc2Yerkes.mat \
 --iout=$myscratch/hipmac/pugsley/Yerkes/struct_brain.nii.gz \
 --fout=$myscratch/hipmac/pugsley/transform/acpc2Yerkes \
 --cout=$myscratch/hipmac/pugsley/transform/acpc2Yerkes_warpcoef \
 --config=$MRCATDIR/config/fnirt_1mm.cnf

 # invert
 fsl_sub invwarp -w $myscratch/hipmac/pugsley/transform/acpc2Yerkes \
  -r $myscratch/hipmac/pugsley/acpc/struct_restore_brain \
  -o $myscratch/hipmac/pugsley/transform/Yerkes2acpc

 applywarp -i $myscratch/hipmac/pugsley/acpc/sub-pugsley_hemi-L_desc03mm-hippoJD_dseg.nii.gz \
 -o $myscratch/hipmac/pugsley/Yerkes/sub-pugsley_hemi-L_desc03mm-hippoJD_dseg.nii.gz \
 --ref=$myscratch/hipmac/peewee/Yerkes/struct_brain.nii.gz \
 -w $myscratch/hipmac/pugsley/transform/acpc2Yerkes.nii.gz --interp=nn

 # surf to Yerkes
 warp=$myscratch/hipmac/pugsley/transform/acpc2Yerkes.nii.gz
 warpinv=$myscratch/hipmac/pugsley/transform/Yerkes2acpc.nii.gz
 for surf in midthickness outer inner; do
     echo $surf
     surfin=$myscratch/hipmac/output/sub-02/hippunfold/sub-pugsley/surf/sub-pugsley_hemi-L_space-corobl_den-0p5mm_label-hipp_${surf}.surf.gii
     surfout=$myscratch/hipmac/pugsley/Yerkes/sub-pugsley_hemi-L_space-corobl_den-0p5mm_label-hipp_${surf}.surf.gii
     wb_command -surface-apply-warpfield $surfin $warpinv $surfout -fnirt $warp
 done


# peewee
flirt -in /vols/Data/km/ahoward/BigMac-clean/MRI/Postmortem/struct/MGE/data/T1-like/T1-like.nii.gz \
-ref /vols/Scratch/neichert/templating/ref_brain.nii.gz \
-omat $myscratch/hipmac/peewee/transform/acpc2Yerkes.mat \
-o $myscratch/hipmac/peewee/Yerkes/T1-like_lin.nii.gz

fnirt --in=/vols/Data/km/ahoward/BigMac-clean/MRI/Postmortem/struct/MGE/data/T1-like/T1-like.nii.gz \
--ref=/vols/Scratch/neichert/templating/ref_brain.nii.gz \
--aff=$myscratch/hipmac/peewee/transform/acpc2Yerkes.mat \
--iout=$myscratch/hipmac/peewee/Yerkes/T1-like.nii.gz \
--fout=$myscratch/hipmac/peewee/transform/acpc2Yerkes \
--cout=$myscratch/hipmac/peewee/transform/acpc2Yerkes_warpcoef \
--config=$MRCATDIR/config/fnirt_1mm.cnf

# invert
fsl_sub invwarp -w $myscratch/hipmac/peewee/transform/acpc2Yerkes \
 -r /vols/Data/km/ahoward/BigMac-clean/MRI/Postmortem/struct/MGE/data/struct_brain.nii.gz \
 -o $myscratch/hipmac/peewee/transform/Yerkes2acpc

# test conversion
applywarp -i /vols/Scratch/neichert/templating/ref_brain.nii.gz \
-o $myscratch/hipmac/peewee/acpc/test.nii.gz \
-r /vols/Data/km/ahoward/BigMac-clean/MRI/Postmortem/struct/MGE/data/struct_brain.nii.gz \
-w $myscratch/hipmac/peewee/transform/Yerkes2acpc --interp=nn

# downsample
flirt -dof 6 -cost corratio -nosearch -in $myscratch/hipmac/peewee/Yerkes/struct_brain.nii.gz \
 -ref /vols/Scratch/neichert/NHPPipelines/global/templates/MacaqueYerkes19_T1w_0.5mm_brain_dedrift.nii.gz \
 -omat $myscratch/hipmac/peewee/transform/Yerkes03_2_05.mat -applyisoxfm 0.5

applywarp -i /vols/Data/km/ahoward/BigMac-clean/MRI/Postmortem/struct/MGE/data/struct_brain.nii.gz \
-o $myscratch/hipmac/peewee/Yerkes/struct_brain.nii.gz \
-r /vols/Scratch/neichert/templating/ref_brain.nii.gz \
-w $myscratch/hipmac/peewee/transform/acpc2Yerkes.nii.gz

applywarp -i $myscratch/hipmac/peewee/acpc/sub-pugsley_hemi-L_desc01mm-hippoJD_dseg.nii.gz \
-o $myscratch/hipmac/peewee/Yerkes/sub-pugsley_hemi-L_desc01mm-hippoJD_dseg.nii.gz \
-r /vols/Scratch/neichert/templating/ref_brain.nii.gz \
-w $myscratch/hipmac/peewee/transform/acpc2Yerkes.nii.gz --interp=nn

# surf to Yerkes
warp=$myscratch/hipmac/peewee/transform/acpc2Yerkes.nii.gz
warpinv=$myscratch/hipmac/peewee/transform/Yerkes2acpc.nii.gz
for surf in midthickness outer inner; do
    echo $surf
    surfin=$myscratch/hipmac/output/sub-03/hippunfold/sub-peewee/surf/sub-peewee_hemi-L_space-corobl_den-unfoldiso_label-hipp_${surf}.surf.gii
    surfout=$myscratch/hipmac/peewee/Yerkes/sub-peewee_hemi-L_space-corobl_den-unfoldiso_label-hipp_${surf}.surf.gii
    wb_command -surface-apply-warpfield $surfin $warpinv $surfout -fnirt $warp
done

# upsample
flirt -dof 6 -cost corratio -nosearch -in $myscratch/hipmac/peewee/acpc/hemi_L_dseg.nii.gz \
 -ref $myscratch/hipmac/peewee/acpc/hemi_L_dseg.nii.gz \
 -omat $myscratch/hipmac/peewee/transform/acpc03_2_01.mat -applyisoxfm 0.1 \
 -out $myscratch/hipmac/peewee/acpc/hemi_L_dseg01.nii.gz

input=/vols/Scratch/neichert/hipmac/BIDS/sub-32/anat/manual_only_R.nii.gz
#premat=$myscratch/hipmac/peewee/transform/acpc03_2_01.mat
output=/vols/Scratch/neichert/hipmac/BIDS/sub-32/anat/manual_only_R_01.nii.gz
ref=/vols/Scratch/neichert/hipmac/BIDS/sub-32/anat/sub-32_hemi-L_dseg.nii.gz
#applywarp -i $input -o $output --premat=$premat -r $ref --interp=nn
$ANTSPATH/antsApplyTransforms -i $input -o $output --interpolation MultiLabel -r $ref

# downmsample again to change mask
input=/host/percy/local_raid/neichert/hipmac/BIDS/sub-32/anat/old.nii.gz
output=/host/percy/local_raid/neichert/hipmac/BIDS/sub-32/anat/old_03.nii.gz
ref=/home/bic/neichert/hipmac/peewee/acpc/struct_brain.nii.gz
antsApplyTransforms -i $input -o $output --interpolation MultiLabel -r $ref
# edit and then upsample, modify with DG


# crop file
padding=5
sub='34'
for hemi in L R; do
  dims=`fslstats $myscratch/hipmac/BIDS/sub-${sub}/anat/manual_${hemi}.nii.gz -w`
  dims=($dims)
  xmin=$((${dims[0]} - $padding))
  xsize=$((${dims[1]} + $padding + $padding))
  ymin=$((${dims[2]} - $padding))
  ysize=$((${dims[3]} + $padding + $padding))
  zmin=$((${dims[4]} - $padding))
  zsize=$((${dims[5]} + $padding + $padding))

  echo 'crop segmentation'
  fslroi $myscratch/hipmac/BIDS/sub-${sub}/anat/manual_${hemi}.nii.gz \
   $myscratch/hipmac/BIDS/sub-${sub}/anat/manual_${hemi}_cropped.nii.gz \
   $xmin $xsize $ymin $ysize $zmin $zsize 0 1

  #ref=/vols/Data/km/ahoward/BigMac-clean/MRI/Postmortem/struct/MGE/data/struct_brain.nii.gz
  ref=/Users/neichert/hipmac/peewee/acpc/struct_brain.nii.gz
  #ref=/data/mica1/03_projects/neichert/hipmac/postmort/peewee/anat/struct_fullfov.nii.gz
  fslroi $ref \
  $myscratch/hipmac/BIDS/sub-${sub}/anat/struct_${hemi}_cropped.nii.gz \
  $xmin $xsize $ymin $ysize $zmin $zsize 0 1

  echo 'upsample to 01'
  flirt \
  -in $myscratch/hipmac/BIDS/sub-${sub}/anat/struct_${hemi}_cropped.nii.gz \
  -ref $myscratch/hipmac/BIDS/sub-${sub}/anat/struct_${hemi}_cropped.nii.gz \
  -applyisoxfm 0.1 \
  -out $myscratch/hipmac/BIDS/sub-${sub}/anat/struct_${hemi}_cropped_01.nii.gz

  input=$myscratch/hipmac/BIDS/sub-${sub}/anat/manual_${hemi}_cropped.nii.gz
  output=$myscratch/hipmac/BIDS/sub-${sub}/anat/manual_${hemi}_cropped_01.nii.gz
  ref=$myscratch/hipmac/BIDS/sub-${sub}/anat/struct_${hemi}_cropped_01.nii.gz
  $ANTSPATH/antsApplyTransforms -i $input -o $output --interpolation MultiLabel -r $ref

  echo 'copy final file'
  imcp $output $myscratch/hipmac/BIDS/sub-${sub}/anat/sub-${sub}_hemi-${hemi}_dseg.nii.gz
done

subj=032104
hemi='L'
padding=10
dims=`fslstats $myscratch/hipmac/BIDS/sub-${subj}/anat/template_${hemi}_03.nii.gz -w`
dims=($dims)
xmin=$((${dims[0]} - $padding))
xsize=$((${dims[1]} + $padding + $padding))
ymin=$((${dims[2]} - $padding))
ysize=$((${dims[3]} + $padding + $padding))
zmin=$((${dims[4]} - $padding))
zsize=$((${dims[5]} + $padding + $padding))
fslroi $myscratch/hipmac/BIDS/sub-${subj}/anat/template_${hemi}_03.nii.gz \
 $myscratch/hipmac/BIDS/sub-${subj}/anat/template_${hemi}_03_cropped.nii.gz \
 $xmin $xsize $ymin $ysize $zmin $zsize 0 1

#ref=/vols/Data/km/ahoward/BigMac-clean/MRI/Postmortem/struct/MGE/data/struct_brain.nii.gz
#ref=/Users/neichert/hipmac/peewee/acpc/struct_brain.nii.gz
#ref=/data/mica1/03_projects/neichert/hipmac/postmort/peewee/anat/struct_fullfov.nii.gz
ref=$myscratch/hipmac/BIDS/sub-${subj}/anat/T1w_acpc_dc_restore.nii.gz
fslroi $ref \
$myscratch/hipmac/BIDS/sub-${subj}/anat/struct_${hemi}_cropped.nii.gz \
$xmin $xsize $ymin $ysize $zmin $zsize 0 1

flirt \
-in $myscratch/hipmac/BIDS/sub-${subj}/anat/struct_${hemi}_cropped.nii.gz \
-ref $myscratch/hipmac/BIDS/sub-${subj}/anat/struct_${hemi}_cropped.nii.gz \
-applyisoxfm 0.1 \
-out $myscratch/hipmac/BIDS/sub-${subj}/anat/struct_${hemi}_cropped_01.nii.gz

# do manual edits and then

input=$myscratch/hipmac/BIDS/sub-${subj}/anat/manual_${hemi}_03.nii.gz
output=$myscratch/hipmac/BIDS/sub-${subj}/anat/manual_${hemi}_01.nii.gz
ref=$myscratch/hipmac/BIDS/sub-${subj}/anat/struct_${hemi}_cropped_01.nii.gz
$ANTSPATH/antsApplyTransforms -i $input -o $output --interpolation MultiLabel -r $ref


# then run!

cd /data/mica3/BigMac_NE/sub-03


qsub cd /data/mica3/BigMac_NE/sub-03/submit.sh

# process bicmac
bval='04'
res='0.6mm'
dwi=/vols/Data/km/ahoward/BigMac-clean/MRI/Postmortem/dwi/b${bval}k/${res}/data.dtifit/dti_RD.nii.gz
out=/vols/Scratch/neichert/hipmac/peewee/acpc/b${bval}k${res}_dti_RD.nii.gz
#dwi=/vols/Scratch/neichert/hipmac/dtd_gamma_MKa.nii.gz
ref=/vols/Data/km/ahoward/BigMac-clean/MRI/Postmortem/struct/MGE/data/struct_brain.nii.gz
#reg=/vols/Data/km/ahoward/BigMac-clean/MRI/Postmortem/dwi/b${bval}k/${res}/reg/dwi_${res}_2_struct_fnirt/dwi_${res}_2_struct_fnirt_field.nii.gz
reg=/vols/Data/km/ahoward/BigMac-clean/MRI/Postmortem/dwi/b${bval}k/${res}/reg/dwi_${res}_2_struct_flirt/dwi_${res}_2_struct_flirt_field.nii.gz
fsl_sub applywarp -i $dwi -o ${out} -r ${ref} -w ${reg}

wb_command -cifti-separate /vols/Scratch/neichert/hipmac/output/sub-03/hippunfold/sub-peewee/surf/sub-peewee_space-corobl_den-unfoldiso_label-hipp_thickness.dscalar.nii COLUMN \
-metric CORTEX_LEFT /vols/Scratch/neichert/hipmac/output/sub-03/hippunfold/sub-peewee/surf/sub-peewee_space-corobl_den-unfoldiso_label-hipp_thickness.func.gii

ribbon='/vols/Scratch/neichert/hipmac/output/sub-03/hippunfold/sub-peewee/anat/sub-peewee_hemi-L_space-corobl_desc-subfields_atlas-bigbrain_dseg.nii.gz'

applywarp -i $ribbon -o /vols/Scratch/neichert/hipmac/seg_01_L_box.nii.gz -r /vols/Scratch/neichert/hipmac/seg_01_L_box.nii.gz \
--premat=/vols/Scratch/neichert/hipmac/nudge.mat

# this did not work with 0.1 resolution
flirt -in $ribbon \
 -out /vols/Scratch/neichert/hipmac/seg_01_L_box.nii.gz \
 -ref /vols/Scratch/neichert/hipmac/seg_L_box.nii.gz -applyisoxfm 0.1

fslmaths /vols/Scratch/neichert/hipmac/CV_thumb_none_0.1_box_L.nii.gz -add /vols/Scratch/neichert/hipmac/seg_01_L_box.nii.gz -sub /vols/Scratch/neichert/hipmac/CV_thumb_none_0.1_box_L.nii.gz /vols/Scratch/neichert/hipmac/seg_01_L_box.nii.gz


for fraction in 1.0 0.9 0.8 0.7 0.6 0.5 0.4 0.3 0.2 0.1 0.0; do
#for fraction in 1.0 0.0; do
  echo $fraction
  outfname=/vols/Scratch/neichert/hipmac/hip_surf/sub-peewee_hemi-L_space-corobl_den-unfoldiso_label-hipp_${fraction}.surf.gii
  wb_command -surface-cortex-layer \
  /vols/Scratch/neichert/hipmac/output/sub-03/hippunfold/sub-peewee/surf/sub-peewee_hemi-L_space-corobl_den-unfoldiso_label-hipp_outer.surf.gii \
  /vols/Scratch/neichert/hipmac/output/sub-03/hippunfold/sub-peewee/surf/sub-peewee_hemi-L_space-corobl_den-unfoldiso_label-hipp_inner.surf.gii \
  $fraction ${outfname}
  #wb_command -surface-smoothing ${outfname} 1 20 ${outfname}
done

wb_command -surface-curvature /vols/Scratch/neichert/hipmac/hip_surf/sub-peewee_hemi-L_space-corobl_den-unfoldiso_label-hipp_0.5.surf.gii \
-mean /vols/Scratch/neichert/hipmac/hip_surf/curvature_0.5.func.gii

wb_command -surface-curvature /vols/Scratch/neichert/hipmac/output/sub-03/hippunfold/sub-peewee/surf/sub-peewee_hemi-L_space-corobl_den-unfoldiso_label-hipp_desc-smoothed_midthickness.surf.gii \
-mean /vols/Scratch/neichert/hipmac/hip_surf/curvature_smoothed_0.5.func.gii

# process ultimate

# start from raw image
cp /vols/Data/postmort/RBM/macaque_BSB_ultimate/70_202210061_MedRes_3D.nifti/NS1_AK5_G_5434_70_mge3d_005_SOS.nifti/MG/sqrtsos001.nii \
/home/fs0/neichert/scratch/hipmac/ultimate/acpc/struct_orig.nii

mri_convert /home/fs0/neichert/scratch/hipmac/ultimate/acpc/struct_orig.nii \
/home/fs0/neichert/scratch/hipmac/ultimate/acpc/struct_orig.nii.gz

# fix labels and axes
sh $MRCATDIR/core/swapdims.sh /home/fs0/neichert/scratch/hipmac/ultimate/acpc/struct_orig.nii.gz \
z x y /home/fs0/neichert/scratch/hipmac/ultimate/acpc/struct.nii.gz 'image'

#rm /home/fs0/neichert/scratch/hipmac/ultimate/acpc/struct_orig.nii.gz

# run structural processing
cd /home/fs0/neichert/scratch/hipmac
img=/home/fs0/neichert/scratch/hipmac/ultimate/acpc/struct.nii.gz
fsl_sub sh $MRCATDIR/core/struct_macaque.sh --subjdir=ultimate --structImg=$img --structdir=ultimate/acpc \
--all

# ultimate to peewee

# lin
flirt -dof 12 -cost corratio \
-in /vols/Scratch/neichert/hipmac/ultimate/acpc/struct_restore_brain.nii.gz \
-ref /vols/Data/km/ahoward/BigMac-clean/MRI/Postmortem/struct/MGE/data/struct_brain.nii.gz \
-omat /vols/Scratch/neichert/hipmac/ultimate/transform/acpc_2_peepwee.mat \
-out /vols/Scratch/neichert/hipmac/peewee/acpc/test.nii.gz

fnirt --in=/vols/Scratch/neichert/hipmac/ultimate/acpc/struct_restore_brain.nii.gz \
--ref=/vols/Data/km/ahoward/BigMac-clean/MRI/Postmortem/struct/MGE/data/struct_brain.nii.gz \
--aff=/vols/Scratch/neichert/hipmac/ultimate/transform/acpc_2_peepwee.mat \
--iout=/vols/Scratch/neichert/hipmac/peewee/acpc/test.nii.gz \
--fout=/vols/Scratch/neichert/hipmac/ultimate/transform/acpc_to_peewee_warp  \
--config=/vols/Scratch/saad/atlases/F99_atlas/config

invwarp -w /vols/Scratch/neichert/hipmac/ultimate/transform/acpc_to_peewee_warp \
 -r /vols/Scratch/neichert/hipmac/ultimate/acpc/struct_restore_brain.nii.gz \
 -o /vols/Scratch/neichert/hipmac/ultimate/transform/peewee_to_acpc_warp


cp /vols/Data/postmort/RBM/macaque_BSB_ultimate/70_202210061_MedRes_3D.nifti/NS1_AK5_G_5434_70_mge3d_005_SOS.nifti/MG/sqrtsos001.nii \
/home/fs0/neichert/scratch/hipmac/ultimate/acpc/struct_orig.nii

# prep mp2rage
mri_convert /vols/Data/postmort/RBM/macaque_BSB_ultimate/NS1_AK5_G_5434_70_MP2RAGE_006.nifti/MG/image001.nii \
/home/fs0/neichert/scratch/hipmac/ultimate/acpc/mp2rage.nii.gz

# fix labels and axes
sh $MRCATDIR/core/swapdims.sh /home/fs0/neichert/scratch/hipmac/ultimate/acpc/mp2rage.nii.gz \
z x y /home/fs0/neichert/scratch/hipmac/ultimate/acpc/mp2rage.nii.gz 'image'

fslroi /home/fs0/neichert/scratch/hipmac/ultimate/acpc/mp2rage.nii.gz /home/fs0/neichert/scratch/hipmac/ultimate/acpc/mp2rage_1.nii.gz 0 1
fslroi /home/fs0/neichert/scratch/hipmac/ultimate/acpc/mp2rage.nii.gz /home/fs0/neichert/scratch/hipmac/ultimate/acpc/mp2rage_1.nii.gz 1 1

rm /home/fs0/neichert/scratch/hipmac/ultimate/acpc/mp2rage.nii.gz

# register mp2rage to struct
flirt -dof 6 -cost corratio -nosearch -in /home/fs0/neichert/scratch/hipmac/ultimate/acpc/mp2rage_1.nii.gz \
-ref /home/fs0/neichert/scratch/hipmac/ultimate/acpc/struct_restore_brain.nii.gz \
-omat /home/fs0/neichert/scratch/hipmac/ultimate/transform/mp2rage_2_acpc.mat \
-out /home/fs0/neichert/scratch/hipmac/ultimate/acpc/mp2rage_1_acpc.nii.gz

applywarp -i /home/fs0/neichert/scratch/hipmac/ultimate/acpc/mp2rage_2.nii.gz \
 -o /home/fs0/neichert/scratch/hipmac/ultimate/acpc/mp2rage_2_acpc.nii.gz \
 -r /home/fs0/neichert/scratch/hipmac/ultimate/acpc/struct_restore_brain.nii.gz \
 --premat=/home/fs0/neichert/scratch/hipmac/ultimate/transform/mp2rage_2_acpc.mat

# register mp2rage to peewee
applywarp -i /home/fs0/neichert/scratch/hipmac/ultimate/acpc/mp2rage_1_acpc.nii.gz  \
 -o /vols/Scratch/neichert/hipmac/peewee/acpc/ultimate_mp2rage_1.nii.gz \
 -r /vols/Data/km/ahoward/BigMac-clean/MRI/Postmortem/struct/MGE/data/struct_brain.nii.gz \
 -w /vols/Scratch/neichert/hipmac/ultimate/transform/acpc_to_peewee_warp

m1=/vols/Scratch/neichert/hipmac/peewee/acpc/ultimate_mp2rage_1.nii.gz
m2=/vols/Scratch/neichert/hipmac/peewee/acpc/ultimate_mp2rage_2.nii.gz

volume=/Users/neichert/hipmac/intersection.nii.gz
surf=/Users/neichert/hipmac/output/sub-03/hippunfold/sub-peewee/surf/sub-peewee_hemi-L_space-corobl_den-unfoldiso_label-hipp_midthickness.surf.gii
proj=/Users/neichert/hipmac/intersection.func.gii
wb_command -volume-to-surface-mapping $volume $surf $proj -enclosing

#cmd from Jordan
python /export03/data/opt/hippunfold/hippunfold/run.py BIDS hippunfold_v1.2.0 participant --modality cropseg --hemi L --output-density unfoldiso 0p5mm --skip_inject_template_labels --keep-work --use-singularity --keep-going -np



for fraction in 1.0 0.9 0.8 0.7 0.6 0.5 0.4 0.3 0.2 0.1 0.0; do
  echo $fraction
  outfname=/vols/Scratch/neichert/hipmac/hip_surf/sub-32_hemi-L_space-corobl_den-unfoldiso_label-hipp_${fraction}.surf.gii
  wb_command -surface-cortex-layer \
  /vols/Scratch/neichert/hipmac/output/sub-32/surf/sub-32_hemi-L_space-corobl_den-unfoldiso_label-hipp_outer.surf.gii \
  /vols/Scratch/neichert/hipmac/output/sub-32/surf/sub-32_hemi-L_space-corobl_den-unfoldiso_label-hipp_inner.surf.gii \
  $fraction ${outfname}
done

hemis='L R'
subs='40 mni'
for sub in $subs; do
    cmd_str=''
    if [[ $sub == 'mni' ]] ; then
        str='unfolded'
    else
        str='unfold'
    fi

    for hemi in $hemis; do
        wb_command -surface-vertex-areas \
            /vols/Scratch/neichert/hipmac/output/sub-${sub}/surf/sub-${sub}_hemi-${hemi}_space-${str}_den-0p5mm_label-hipp_midthickness.surf.gii \
            /vols/Scratch/neichert/hipmac/sub-${sub}_${hemi}_vertex_areas_unfold_0p5mm.func.gii
        if [[ $hemi == 'L' ]]; then
            cmd_str="$cmd_str -left-metric sub-${sub}_${hemi}_vertex_areas_unfold_0p5mm.func.gii"
        elif [[ $hemi == 'R' ]]; then
            cmd_str="$cmd_str -right-metric sub-${sub}_${hemi}_vertex_areas_unfold_0p5mm.func.gii"
        fi
    done # hemi
    out=/vols/Scratch/neichert/hipmac/sub-${sub}_vertex_areas_unfold_0p5mm.dscalar.nii
    wb_command -cifti-create-dense-scalar $out  $cmd_str
    echo $out
done
