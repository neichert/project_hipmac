#!/bin/bash
set -e
umask u+rw,g+rw # give group read/write permissions to all new files


# before: RUN NHP HCP PIPELINE FOR T1w and T2w

# How to run this script:
#  sh $myCode/RS/rest_vol_wrapper.sh

dataset='Jerome'
origdir=/vols/Data/sj/Nicole/${dataset}
SD=$origdir/derivatives
scriptsDir=/vols/Scratch/neichert/myCode/RS
logdir=$origdir/logs
onJalapeno='Yes'
RUN=''
cmd_str=''

if [[ $dataset = 'site-ucdavis' ]] ; then
    subj_list_tot="sub-032125 sub-032126 sub-032127 sub-032128 sub-032129 \
                   sub-032130 sub-032131 sub-032132 sub-032133 sub-032134 \
                   sub-032135 sub-032136 sub-032137 sub-032138 sub-032139 \
                   sub-032140 sub-032141 sub-032142 sub-032143"
elif [[ $dataset = 'site-newcastle' ]] ; then
    subj_list_tot="sub-032097 sub-032100 sub-032102 sub-032104 sub-032105 \
                   sub-032106 sub-032107 sub-032108 sub-032109 sub-032110"
elif [[ $dataset = 'Jerome' ]] ; then
    subj_list_tot="orson orvil puzzle sadif tickle tim travis valhalla voodoo winky"
fi

task_list="run-1"
steps_list="migp_s"
subj_list=$subj_list_tot
DoSubmit='Yes'

for task in $task_list; do
  for step_name in $steps_list; do
    # -----------------------
    # FIELDMAP
    # -----------------------
    if [[ $step_name == 'fieldmap' ]] ; then
      [[ $DoSubmit == "Yes" ]] && RUN='fsl_sub -q veryshort.q -N fmap -l '$logdir

    # -----------------------
    # BET FUNC
    # -----------------------
    elif [[ $step_name == 'bet_func' ]] ; then
      [[ $DoSubmit == "Yes" ]] && RUN='fsl_sub -q long.q -N bet -l '$logdir

    # -----------------------
    # BIASCORRECT FUNC
    # -----------------------
    elif [[ $step_name == 'bias_correct' ]] ; then
      [[ $DoSubmit == "Yes" ]] && RUN='fsl_sub -q short.q -N bias -j bet -l '${logdir}

    # -----------------------
    # REGISTRATION
    # -----------------------
    elif [[ $step_name == 'derive_registration' ]] ; then
        [[ $DoSubmit == "Yes" ]] && RUN='fsl_sub -q veryshort.q -N reg -l '${logdir}

    # -----------------------
    # FEAT
    # -----------------------
    elif [[ $step_name == 'feat' ]] ; then
        echo 'no fsl_sub'

    # -----------------------
    # CLEANING
    # -----------------------
    elif [[ $step_name == 'melodic_1_run' ]] ; then
      [[ $DoSubmit == "Yes" ]] && RUN='fsl_sub -q veryshort.q -N melodic1 -j feat5_stop -l '${logdir}


    elif [[ $step_name == 'melodic_1_clean' ]] ; then
      [[ $DoSubmit == "Yes" ]] && RUN='fsl_sub -q veryshort.q -N melodic1c -l '${logdir}

    elif [[ $step_name == 'melodic_2_run' ]] ; then
      [[ $DoSubmit == "Yes" ]] && RUN='fsl_sub -q veryshort.q -N melodic2 -j melodic1c -l '${logdir}

elif [[ $step_name == 'melodic_2_clean' ]] ; then
      [[ $DoSubmit == "Yes" ]] && RUN='fsl_sub -q veryshort.q -N melodic2c -l '${logdir}

    # -----------------------
    # REGISTER STANDARD
    # -----------------------
    elif [[ $step_name == 'register_standard' ]] ; then
        [[ $DoSubmit == "Yes" ]] && RUN='fsl_sub -q veryshort.q -N regstd -l '${logdir}


    elif [[ $step_name == 'apply_standard' ]] ; then
        [[ $DoSubmit == "Yes" ]] && RUN='fsl_sub -q short.q -N regstd_a -j regstd -l '${logdir}
   

    # here actually run the command
    for subj in $subj_list; do
        echo 'do:' $subj $step_name $task
        ${RUN} sh $scriptsDir/rest_vol.sh ${dataset} ${subj} ${step_name} ${task}
    done
    # run secondary command to hold
    echo 'seconary command...'
    $cmd_str
    
  done # steps
done # task
