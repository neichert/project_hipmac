#!/bin/bash

# Wrapper to run NHP-HCP structural pipeline and surface fMRI processing
# How to run this script:
#  sh /vols/Scratch/neichert/NHPPipelines/Examples/Scripts/the_wrapper_of_wrappers.sh


set -e
umask u+rw,g+rw # give group read/write permissions to all new files

# Before running the pipeline for the first time make all scripts executable
#chmod u+x -R /vols/Scratch/neichert/NHPPipelines/*
# but don't track this in Git:
# git config core.filemode false

# Requirements for this script
#  installed versions of: FSL5.0.2 or higher , FreeSurfer (version 5.2 or higher) , gradunwarp (python code from MGH)
#  environment: FSLDIR , FREESURFER_HOME , HCPPIPEDIR , CARET7DIR , PATH (for gradient_unwarp.py)

dataset='Jerome'
origdir=/vols/Data/sj/Nicole/${dataset}
SD=$origdir/derivatives
ScriptsDir=/vols/Scratch/neichert/NHPPipelines/Examples/Scripts
logdir=/vols/Data/sj/Nicole/${dataset}/logs

EnvironmentScript="$ScriptsDir/SetUpHCPPipelineNHP.sh"
. ${EnvironmentScript}

cd $origdir


if [[ $dataset = 'site-ucdavis' ]] ; then
    subj_list_tot="sub-032125 sub-032126 sub-032127 sub-032128 sub-032129 \
                   sub-032130 sub-032131 sub-032132 sub-032133 sub-032134 \
                   sub-032135 sub-032136 sub-032137 sub-032138 sub-032139 \
                   sub-032140 sub-032141 sub-032142 sub-032143"
    task_list="run-1_func_cleaned run-2_func_cleaned"
elif [[ $dataset = 'site-newcastle' ]] ; then
    subj_list_tot="sub-032097 sub-032100 sub-032102 sub-032104 sub-032105 \
                   sub-032106 sub-032107 sub-032108 sub-032109 sub-032110"
    task_list="run-1_func_cleaned run-2_func_cleaned"

elif [[ $dataset = 'Jerome' ]] ; then
    subj_list_tot='orson orvil puzzle sadif tickle tim travis valhalla voodoo winky'
    task_list="run-1_func_cleaned"
fi

Task="INIT" # "INIT" "PRE" "FREE" "POST" "fMRIS"
subj_list="orvil puzzle sadif tickle tim travis valhalla voodoo winky"

# -----------------------
# GET INITIAL BRAIN MASK
# -----------------------
if [[ $Task = "INIT" ]] ; then
  RUN1='fsl_sub -q veryshort.q -N INIT -l '$logdir
  RUN2='fsl_sub -q veryshort.q -j INIT -N INIT_check -l '$logdir
  cmd_str=''
  for subj in $subj_list; do
    ${RUN1} $ScriptsDir/PrePreFreeSurfer_NE.sh $origdir $subj
    D=$SD/$subj/RawData
    cmd_str="$cmd_str $D/${subj}_ses-00_run-1_T1w_MPR1 $D/T1w_brain_mask.nii.gz"
  done
  ${RUN2} slicesdir -o $cmd_str
fi


# -----------------------
# RUN PRE STAGE
if [[ $Task = "PRE" ]] ; then
  $ScriptsDir/PreFreeSurferPipelineBatchNHP.sh $SD "${subj_list[@]}"

  # check pre stage
  RUN='fsl_sub -q veryshort.q -j PRE -N PRE_check -l '$logdir
  cmd_str=''
  for subj in $subj_list; do
    TD=$SD/$subj/T1w
    cmd_str="$cmd_str $TD/T1w_acpc_dc_restore.nii.gz $TD/T1w_acpc_brain_mask.nii.gz"
  done
  ${RUN} slicesdir -o $cmd_str
fi


if [[ $Task = "FREE" ]] ; then
  $ScriptsDir/FreeSurferPipelineBatchNHP.sh $SD "${subj_list[@]}"

  # to hold
  RUN='fsl_sub -q veryshort.q -N FREE_check -j FREE -l '$logdir
  RUN2='fsl_sub -q veryshort.q -j FREE_check -N FREE_c2 -l '$logdir
  cmd_str=''
  for subj in $subj_list; do
    ${RUN} sh $ScriptsDir/FreeSurfer_check_NE.sh $SD $subj
    D=$SD/$subj/T1w
    cmd_str="$cmd_str $D/T1w_acpc_dc_restore.nii.gz $D/WM.nii.gz"
  done
  ${RUN2} slicesdir -o $cmd_str
fi

if [[ $Task = "POST" ]] ; then
  $ScriptsDir/PostFreeSurferPipelineBatchNHP.sh $SD $subj_list
fi


if [[ $Task = "fMRIS" ]] ; then
  $ScriptsDir/GenericfMRISurfaceProcessingPipelineBatchNHP.sh $SD "${subj_list[@]}" "${task_list[@]}"
fi
