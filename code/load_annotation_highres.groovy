import java.lang.reflect.Type;
def gson = GsonTools.getInstance(true)
def imageData = QPEx.getCurrentImageData()
def server = imageData.getServer()
import java.nio.file.Paths

// output directory
def rootdir = '/home/fs0/neichert/scratch/hipmac/annotations/'

// make new directory for this slice
String path = server.getPath()
ppath = path.split('/')
//def slicepath =  Paths.get(ppath[-6], ppath[-5], ppath[-4], ppath[-3], ppath[-2])
def slicepath =  Paths.get(ppath[-5], ppath[-4], ppath[-3], 'structureTensor',  ppath[-2]+ppath[-1][5]+'_ST_150')

fname = rootdir + slicepath + '/annotations_highres.geojson'
//fname = '/home/fs0/neichert/scratch/hipmac/annotations/test.geojson'
print fname

def myjson = new File(fname).text

// Read the annotations
def type = new com.google.gson.reflect.TypeToken<List<qupath.lib.objects.PathObject>>() {}.getType()

def deserializedAnnotations = gson.fromJson(myjson, type)
addObjects(deserializedAnnotations)  