def imageData = QPEx.getCurrentImageData()
def server = imageData.getServer()
import java.nio.file.Paths

// output directory
def rootdir = '/home/fs0/neichert/scratch/hipmac/annotations/'

// make new directory for this slice
String path = server.getPath()
ppath = path.split('/')


def slicepath =  Paths.get(ppath[-5], ppath[-4], ppath[-3], 'structureTensor',  ppath[-2]+ppath[-1][5]+'_ST_150')

outdir = rootdir + slicepath
def newContainer = new File(outdir)
newContainer.mkdirs()

// write out
def annotations = getAnnotationObjects()
boolean prettyPrint = false
def gson = GsonTools.getInstance(prettyPrint)


fname = outdir + '/test.geojson'
print fname
File file = new File(fname)
file.withWriter('UTF-8') {
    gson.toJson(annotations,it)
}