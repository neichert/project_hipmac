import qupath.lib.gui.viewer.overlays.BufferedImageOverlay
import javax.imageio.ImageIO
import java.lang.reflect.Type;
import java.nio.file.Paths

// remove existing annotations
def annotations = getAnnotationObjects()
removeObjects(annotations, false)


// def path = "/vols/Data/km/ahoward/BigMac-clean/Microscopy/Gallyas/Anterior/H090x/H090a_structureTensor/RGB_thumb.tif"

String path1 = "/vols/Data/BigMac/Microscopy/Cresyl/Posterior/CV"
String path2 = "067"
String path3 = "x/structureTensor/CV"
String path4 = "a_ST_150/CV"
String path5 = "a_thumb.tif"

def path = path1 + path2 + path3 + path2 + path4 + path2 + path5

print path 
def img = ImageIO.read(new File(path))
def viewer = getCurrentViewer()
def overlay = new BufferedImageOverlay(viewer, img)


def gson = GsonTools.getInstance(true)
def imageData = QPEx.getCurrentImageData()
def server = imageData.getServer()


// output directory
def rootdir = '/home/fs0/neichert/scratch/hipmac/annotations/'


ppath = path.split('/')
def slicepath =  Paths.get(ppath[-6], ppath[-5], ppath[-4], ppath[-3], ppath[-2])


fname = rootdir + slicepath + '/annotations.geojson'

print fname

def myjson = new File(fname).text

// Read the annotations
def type = new com.google.gson.reflect.TypeToken<List<qupath.lib.objects.PathObject>>() {}.getType()

def deserializedAnnotations = gson.fromJson(myjson, type)
addObjects(deserializedAnnotations)  
