import java.lang.reflect.Type;
def gson = GsonTools.getInstance(true)
def imageData = QPEx.getCurrentImageData()
def server = imageData.getServer()
import java.nio.file.Paths

// output directory
def rootdir = '/home/fs0/neichert/scratch/hipmac/annotations/'

// make new directory for this slice
String path = server.getPath()
ppath = path.split('/')
def slicepath =  Paths.get(ppath[-6], ppath[-5], ppath[-4], ppath[-3], ppath[-2])


fname = rootdir + slicepath + '/annotations_revision.geojson'

print fname

def myjson = new File(fname).text

// Read the annotations
def type = new com.google.gson.reflect.TypeToken<List<qupath.lib.objects.PathObject>>() {}.getType()

def deserializedAnnotations = gson.fromJson(myjson, type)
addObjects(deserializedAnnotations)  