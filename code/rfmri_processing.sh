#!/bin/bash
set -e
umask u+rw,g+rw # give group read/write permissions to all new files

dataset=$1
subj=$2
step_name=$3
task=$4
subj_list=$5
if [[ -z "$task" ]] ; then
    task='FH'
fi

if [[ -z "$subj_list" ]] ; then
    subj_list=''
fi

origdir=/vols/Data/sj/Nicole/${dataset}
SD=$origdir/derivatives
sdir=/vols/Scratch/neichert/myCode/RS
onJalapeno='Yes'

if [[ "$dataset" = *"site-newcastle"* ]] ; then
    case $subj in
        'sub-032097' )
            ses='002' ;;
         'sub-032100' )
            ses='003' ;;
         'sub-032102')
            ses='002' ;;
         'sub-032104')
            ses='003' ;;
            *)
            ses='001' ;;
    esac
elif [[ "$dataset" = *"site-ucdavis"* ]] ; then
    ses='001'
elif [[ "$dataset" = *"Jerome"* ]] ; then
    ses='001'
fi

# root BIDS data directory
DD=${origdir}/${subj}/ses-${ses}
# root derivative directoy
OD=${SD}/${subj}
# outputs for resting state derived data
D=${SD}/${subj}/rest/${task}
# output directory for HCP-processed T1w
TD=${SD}/${subj}/T1w

echo "do $step_name"

remove_brackets(){
	echo $* | sed s@"\["@@g | sed s@"\]"@@g | sed s@" "@@g
}

if [[ $step_name == 'test_inputs' ]] ; then
  echo 'subj: ' $subj
  echo 'step_name: ' $step_name
  echo 'task: ' $task
  echo 'subj_list': ${subj_list[@]}
  echo 'onJalapeno: ' $onJalapeno

# -----------------------
# PROCESS FIELDMAP
# -----------------------
elif [[ $step_name == 'fieldmap' ]] ; then
    # Create output folders
    for myfolder in $SD/$subj/rest/FH/transforms $SD/$subj/rest/HF/transforms $SD/$subj/fmap ; do
      if [ ! -d $myfolder ]; then
        mkdir -p $myfolder
      else
        echo $myfolder already exists
      fi
    done

    echo 'prepare fieldmap'
    # take first volume of magnitude image
    fslroi $DD/fmap/${subj}_ses-001_run-1_magnitude1.nii.gz $OD/fmap/${subj}_ses-001_run-1_magnitude1 0 1
    # get brainmask to fmap space (assuming both are overlapping in the native space)
    convert_xfm -omat $OD/T1w/xfms/acpc2orig.mat -inverse $OD/T1w/xfms/acpc.mat
    applywarp -i $OD/T1w/T1w_acpc_brain_mask.nii.gz -o $OD/RawData/T1w_raw_brain_mask.nii.gz \
    -r $OD/RawData/${subj}_ses-001_run-1_T1w_MPR1.nii.gz --premat=$OD/T1w/xfms/acpc2orig.mat --interp=nn
    applywarp -i $OD/RawData/T1w_raw_brain_mask.nii.gz -o $OD/fmap/T1w_raw_brain_mask.nii.gz \
    -r $OD/fmap/${subj}_ses-001_run-1_magnitude1 --usesqform
    # bet magnitude image
    fslmaths $OD/fmap/${subj}_ses-001_run-1_magnitude1 -mas $OD/fmap/T1w_raw_brain_mask.nii.gz $OD/fmap/${subj}_ses-001_run-1_magnitude1_brain.nii.gz
    fslmaths $OD/fmap/${subj}_ses-001_run-1_magnitude1_brain.nii.gz -ero -kernel box 3 $OD/fmap/${subj}_ses-001_run-1_magnitude1_brain.nii.gz

    # prepare fieldmap
    fsl_prepare_fieldmap SIEMENS $DD/fmap/${subj}_ses-001_run-2_phasediff.nii.gz \
    $OD/fmap/${subj}_ses-001_run-1_magnitude1_brain.nii.gz $OD/fmap/fmap_rads 2.45

    echo 'do registration with T1w acpc'
    flirt -in $OD/fmap/${subj}_ses-001_run-1_magnitude1_brain -ref $TD/T1w_acpc_dc_restore_brain -dof 6 -omat $TD/xfms/fieldmap2str_init.mat -out ${OD}/fmap/fieldmap2str_init
    flirt -in $OD/fmap/${subj}_ses-001_run-1_magnitude1 -ref $TD/T1w_acpc_dc_restore_brain -dof 6 -init $TD/xfms/fieldmap2str_init.mat -omat $TD/xfms/fieldmap2str.mat -out ${OD}/fmap/fieldmap2str -nosearch
    applywarp -i $OD/fmap/${subj}_ses-001_run-1_magnitude1 -o ${OD}/fmap/fieldmap2str -r $TD/T1w_acpc_dc_restore_brain --premat=$TD/xfms/fieldmap2str_init.mat
    applywarp -i $OD/fmap/fmap_rads -o ${OD}/fmap/fieldmaprads2str -r $TD/T1w_acpc_dc_restore_brain --premat=$TD/xfms/fieldmap2str_init.mat
    applywarp -i $OD/fmap/${subj}_ses-001_run-1_magnitude1_brain -o ${OD}/fmap/fieldmapbrain2str -r $TD/T1w_acpc_dc_restore_brain --premat=$TD/xfms/fieldmap2str_init.mat

# -----------------------
# BET FUNC
# -----------------------
elif [[ $step_name == 'bet_func' ]] ; then
    echo 'copy raw data'

    dir=$SD/$subj/rest/${task}/transforms
    if [ ! -d $dir ]; then
        mkdir -p $dir
    else
        echo $dir already exists
    fi

    if [[ $dataset == 'Jerome' ]] ; then
        echo 'copy from previous pipeline'

        img=/vols/Scratch/neichert/monkeyRS/${subj}/functional/raw_ref_brain_mask.nii.gz
        sh $MRCATDIR/core/swapdims.sh $img x y z ${D}/raw_brain_mask.nii.gz 'image'
        echo ${D}/raw_brain_mask.nii.gz

        img=/vols/Scratch/neichert/monkeyRS/${subj}/functional/raw_ref_brain_mask_strict.nii.gz
        sh $MRCATDIR/core/swapdims.sh $img x y z ${D}/raw_brain_mask_strict.nii.gz 'image'

        img=/vols/Scratch/neichert/monkeyRS/${subj}/functional/raw.nii.gz
        sh $MRCATDIR/core/swapdims.sh $img x y z ${D}/raw.nii.gz 'image'
    else
        # ucdavis
        if [[ $task == 'HF' ]]; then
          raw_scan=${DD}/func/${subj}_ses-${ses}_task-resting_run-1_bold.nii.gz
        elif [[ $task == 'HF' ]]; then
          raw_scan=${DD}/func/${subj}_ses-${ses}_task-resting_acq-RevPol_run-1_bold.nii.gz
         # newcastle: run-1, run-2
        else
          raw_scan=${DD}/func/${subj}_ses-${ses}_task-resting_${task}_bold.nii.gz
        fi

        imcp ${raw_scan} ${D}/raw.nii.gz

        # get ref vol
        funcscan=${D}/raw
        dim4=`fslval $funcscan dim4`
        mid=`echo $dim4/2|bc`
        fslroi $funcscan ${funcscan}_ref $mid 1

        echo 'bet ref vol using mrCat'
        sh ${MRCATDIR}/core/bet_macaque.sh ${D}/raw_ref ${D}/raw -t T2star -s 70
        fslmaths ${D}/raw_brain_mask -ero -ero ${D}/raw_brain_mask_strict
    fi

# -----------------------
# BIASCORRECT FUNC
# -----------------------
elif [[ $step_name == 'bias_correct' ]] ; then
    # needs brainmask from above
    sh $MRCATDIR/pipelines/rfmri_macaque/rsn_biascorr.sh ${D}/raw ${D}/raw_restore

# -----------------------
# REGISTRATION FUNC2ANAT
# -----------------------
elif [[ $step_name == 'derive_registration' ]] ; then
    if [[ $task == 'FH' ]] ; then
        pedir='y'
    else
        pedir='y-'
    fi

    convert_xfm -omat $TD/xfms/acpc2orig.mat -inverse $TD/xfms/acpc.mat

    if [[ "$dataset" = *"newcastle"* ]] ; then
      # get T1w in epi space to drive registration
      applywarp -i $TD/T1w_acpc_dc_restore_brain -o $D/transforms/tmp --premat=$TD/xfms/acpc2orig.mat -r ${TD}/T1w_brain
      applywarp -i $D/transforms/tmp -o $D/transforms/reg_ref_brain -r ${D}/raw_ref --usesqform
      rm $D/transforms/tmp.nii.gz

        if [[ "$subj" = *"sub-032107"* ]] ; then
             # get initial registration
             flirt -in $D/transforms/reg_ref_brain -ref $TD/T1w_acpc_dc_restore_brain.nii.gz -omat $D/transforms/reg_ref2acpc.mat
             convert_xfm -omat $D/transforms/acpc2reg_ref.mat -inverse $D/transforms/reg_ref2acpc.mat

             echo 'do flirt'
             funcscan=${D}/raw_restore
             dim4=`fslval $funcscan dim4`
             mid=`echo $dim4/2|bc`
             fslroi $funcscan ${funcscan}_ref $mid 1

             flirt -in ${D}/raw_restore_ref -ref $TD/T1w_acpc_dc_restore_brain \
             -init $D/transforms/reg_ref2acpc.mat -o $D/transforms/epi2T1w -dof 6 \
             -omat $D/transforms/epi2T1w.mat -cost mutualinfo

             echo 'apply registration'
             # T1w to epi
             convert_xfm -omat $D/transforms/T1w2epi.mat -inverse $D/transforms/epi2T1w.mat
             applywarp -i $TD/T1w_acpc_dc_restore_brain -o $D/T1w_brain --premat=$D/transforms/T1w2epi.mat -r ${D}/raw_ref

        else
            flirt -in $D/transforms/reg_ref_brain -ref $TD/T1w_acpc_dc_restore_brain.nii.gz -omat $D/transforms/epi2T1w.mat
            convert_xfm -omat $D/transforms/T1w2epi.mat -inverse $D/transforms/epi2T1w.mat

            echo 'apply registration'
            # T1w to epi
            convert_xfm -omat $D/transforms/T1w2epi.mat -inverse $D/transforms/epi2T1w.mat
            applywarp -i $TD/T1w_acpc_dc_restore_brain -o $D/T1w_brain --premat=$D/transforms/T1w2epi.mat -r ${D}/raw_ref
            applywarp -i $D/raw_restore_mean -o $D/transforms/epi2T1w.nii.gz --premat=$D/transforms/epi2T1w.mat -r $TD/T1w_acpc_dc_restore_brain.nii.gz
        fi

    elif [[ "$dataset" = *"davis"* ]] ; then

        echo 'prepare flirt registration'

        # get T1w in epi space to drive registration
        applywarp -i $TD/T1w_acpc_dc_restore_brain -o $D/transforms/tmp --premat=$TD/xfms/acpc2orig.mat -r ${TD}/T1w_brain
        applywarp -i $D/transforms/tmp -o $D/transforms/reg_ref_brain -r ${D}/raw_ref --usesqform
        rm $D/transforms/tmp.nii.gz

        flirt -in $D/transforms/reg_ref_brain -ref $TD/T1w_acpc_dc_restore_brain.nii.gz -omat $D/transforms/reg_ref2acpc.mat
        convert_xfm -omat $D/transforms/acpc2reg_ref.mat -inverse $D/transforms/reg_ref2acpc.mat

        # get head and WM
        applywarp -i $TD/T1w_acpc_dc_restore -o $D/transforms/reg_ref --premat=$D/transforms/acpc2reg_ref.mat -r ${D}/raw_ref
        applywarp -i $TD/WM -o $D/transforms/regWM --premat=$D/transforms/acpc2reg_ref.mat -r ${D}/raw_ref --interp=nn

        echo 'do flirt'
        funcscan=${D}/raw_restore
        dim4=`fslval $funcscan dim4`
        mid=`echo $dim4/2|bc`
        fslroi $funcscan ${funcscan}_ref $mid 1

        flirt -in ${D}/raw_restore_ref -ref $TD/T1w_acpc_dc_restore_brain \
        -init $D/transforms/reg_ref2acpc.mat -o $D/transforms/epi2T1w -dof 6 \
        -omat $D/transforms/epi2T1w.mat \
        -fieldmap $D/../../fmap/fieldmaprads2str \
        -fieldmapmask $D/../../fmap/fieldmapbrain2str -cost mutualinfo

        echo 'apply registration'
        # T1w to epi
        convert_xfm -omat $D/transforms/T1w2epi.mat -inverse $D/transforms/epi2T1w.mat
        applywarp -i $TD/T1w_acpc_dc_restore_brain -o $D/T1w_brain --premat=$D/transforms/T1w2epi.mat -r ${D}/raw_ref
        # fmap to epi
        convert_xfm -omat $D/transforms/fmap2epi.mat -concat $D/transforms/T1w2epi.mat $TD/xfms/fieldmap2str_init.mat
        applywarp -i $D/../../fmap/fmap_rads -o $D/transforms/fmap_rads --premat=$D/transforms/fmap2epi.mat -r ${D}/raw_ref
        applywarp -i $D/../../fmap/${subj}_ses-001_run-1_magnitude1_brain -o $D/transforms/fmap_mag_brain --premat=$D/transforms/fmap2epi.mat -r ${D}/raw_ref

    elif [[ "$dataset" = *"Jerome"* ]]; then
        echo 'prepare flirt registration'

        # get version from previous pipeline to initiate
        img=/vols/Scratch/neichert/monkeyRS/${subj}/functional/struct_restore.nii.gz
        sh $MRCATDIR/core/swapdims.sh $img x y z $D/transforms/reg_ref  'image'

        fslmaths $D/transforms/reg_ref -mas $D/raw_brain_mask.nii.gz $D/transforms/reg_ref_brain

        flirt -dof 6 -in $D/transforms/reg_ref_brain -ref $TD/T1w_acpc_dc_restore_brain.nii.gz \
        -omat $D/transforms/reg_ref2acpc.mat -o $D/transforms/tmp.nii.gz

        convert_xfm -omat $D/transforms/acpc2reg_ref.mat -inverse $D/transforms/reg_ref2acpc.mat

        if [[ $subj == 'orvil' ]] ; then
            echo 'keep init reg'
            cp $D/transforms/reg_ref2acpc.mat $D/transforms/epi2T1w.mat
        else
            echo 'do flirt'
            funcscan=${D}/raw_restore
            dim4=`fslval $funcscan dim4`
            mid=`echo $dim4/2|bc`
            fslroi $funcscan ${funcscan}_ref $mid 1

            flirt -in ${D}/raw_restore_ref -ref $TD/T1w_acpc_dc_restore_brain \
            -init $D/transforms/reg_ref2acpc.mat -o $D/transforms/epi2T1w -dof 6 \
            -omat $D/transforms/epi2T1w.mat -cost mutualinfo
        fi

        # T1w to epi
        convert_xfm -omat $D/transforms/T1w2epi.mat -inverse $D/transforms/epi2T1w.mat
        applywarp -i $TD/T1w_acpc_dc_restore_brain -o $D/T1w_brain --premat=$D/transforms/T1w2epi.mat -r ${D}/raw_restore_mean
        echo "done " $D/T1w_brain
    fi

    fslmaths $D/T1w_brain -bin $D/final_brain_mask.nii.gz


# -----------------------
# FEAT
# -----------------------
elif [[ $step_name == 'feat' ]] ; then

    # use previous design file as template
    cp /vols/Data/sj/Nicole/site-ucdavis/derivatives/group/rest/template.fsf $D/design.fsf

    # change the file paths and feat settings
    if [[ $dataset == 'site-ucdavis' ]]; then
        st=5 #Interleaved
    else
        st=0 #None
    fi

    TR=`fslval $D/raw_restore.nii.gz "pixdim4"`
    nvols=`fslval $D/raw_restore.nii.gz "dim4"`
    sed -i "s/site-ucdavis/${dataset}/g" $D/design.fsf
    sed -i "s/sub-032128/${subj}/g" $D/design.fsf
    sed -i "s/HF/${task}/g" $D/design.fsf
    sed -i "s/set fmri(tr) 1.600000/set fmri(tr) $TR/g" $D/design.fsf
    sed -i "s/set fmri(npts) 250/set fmri(npts) $nvols/g" $D/design.fsf
    sed -i "s/set fmri(st) 5/set fmri(st) $st/g" $D/design.fsf
    # run
    feat $D/design.fsf

# -----------------------
# MELODIC
# -----------------------

# just test
bad_comps=`cat $D/feat.feat/filtered_func_data.ica/hand_labels_noise.txt | tail -n 1`
#

elif [[ $step_name == 'melodic_1_run' ]] ; then
  melodic -i $D/feat.feat/filtered_func_data --nobet --verbose --eps=0.01 --seed=1
elif [[ $step_name == 'melodic_1_clean' ]] ; then
  bad_comps=`cat $D/feat.feat/filtered_func_data.ica/hand_labels_noise.txt | tail -n 1`
  fsl_regfilt -i $D/feat.feat/filtered_func_data -o $D/feat.feat/filtered_func_data_cleaned \
  -d $D/feat.feat/filtered_func_data.ica/melodic_mix -f "`remove_brackets $bad_comps`" -v
  echo 'wrote out ' $D/feat.feat/filtered_func_data_cleaned
elif [[ $step_name == 'melodic_2_run' ]] ; then
  # Run melodic again with BrainMask
  echo 'apply brain mask'
  fslmaths $D/feat.feat/filtered_func_data_cleaned -mas $D/final_brain_mask $D/feat.feat/filtered_func_data_cleaned
  melodic -i $D/feat.feat/filtered_func_data_cleaned --nobet --seed=1 -d 20
elif [[ $step_name == 'melodic_2_clean' ]] ; then
  bad_comps=`cat $D/feat.feat/filtered_func_data_cleaned.ica/hand_labels_noise.txt | tail -n 1`
  fsl_regfilt -i $D/feat.feat/filtered_func_data_cleaned -o $D/func_cleaned \
  -d $D/feat.feat/filtered_func_data_cleaned.ica/melodic_mix -f "`remove_brackets $bad_comps`" -v
  echo 'wrote out ' $D/func_cleaned

# -----------------------
# DTSERIES (redundant, don't use)
# -----------------------
elif [[ $step_name == 'dtseries' ]] ; then
    echo 'transform surfaces'
    for hemi in L R; do
      for surf in midthickness white pial; do
          wb_command -surface-apply-affine $TD/fsaverage_LR10k/${subj}.${hemi}.${surf}.10k_fs_LR.surf.gii \
          $D/transforms/T1w2epi.mat $D/${hemi}.${surf}.10k.surf.gii -flirt \
          $TD/T1w_acpc_dc_restore_brain.nii.gz $D/raw_restore_mean.nii.gz
      done
    done

    echo 'get native atlas ROIs'
    applywarp -i $TD/wmparc -o $D/wmparc -r $D/raw_restore_mean --premat=$D/transforms/T1w2epi.mat --interp=nn
    wb_command -volume-label-import $D/wmparc.nii.gz \
      $SD/group/NE_Structures_LookUp.txt \
      $D/wmparc.nii.gz -discard-others -drop-unused-labels

    wb_command -cifti-create-dense-scalar $D/roi.10k_fs_LR.dscalar.nii \
    -left-metric $TD/../MNINonLinear/fsaverage_LR10k/${subj}.L.MyelinMap.10k_fs_LR.func.gii \
    -right-metric $TD/../MNINonLinear/fsaverage_LR10k/${subj}.R.MyelinMap.10k_fs_LR.func.gii \
    -volume $D/wmparc.nii.gz $D/wmparc.nii.gz

    wb_command -cifti-math "x>0" $D/roi.10k_fs_LR.dscalar.nii -var x $D/roi.10k_fs_LR.dscalar.nii
    echo 'done applying registration'
    echo 'remove copy of raw'
    rm $D/raw.nii.gz

    echo 'vol2surf'
    sh $MRCATDIR/pipelines/rfmri_macaque/rsn_vol2surf.sh \
    --funcimg=$D/func_cleaned.nii.gz \
    --projmethod=ribbon \
    --surfname='10k' \
    --surfleft=$D/L.midthickness.10k.surf.gii \
    --surfright=$D/R.midthickness.10k.surf.gii \
    --volspace=T1w

# -----------------------
# process_dtseries (redundant, don't use)
# -----------------------
elif [[ $step_name == 'process_dtseries' ]] ; then
  echo 'process dtseries'
  wb_command -cifti-smoothing $D/func_cleaned.10k.dtseries.nii 3 2 COLUMN \
    $D/func_cleaned.10k.dtseries.nii \
    -left-surface $D/L.midthickness.10k.surf.gii \
    -right-surface $D/R.midthickness.10k.surf.gii \

  # mask medial wall
  wb_command -cifti-math "(data * mask)" $D/func_cleaned.10k.dtseries.nii \
   -var 'data' $D/func_cleaned.10k.dtseries.nii \
   -var 'mask' $D/roi.10k_fs_LR.dscalar.nii -select 1 1 -repeat

  # demean
  wb_command -cifti-reduce $D/func_cleaned.10k.dtseries.nii MEAN $D/tmp.dscalar.nii
  wb_command -cifti-math "(data-mean)" $D/func_cleaned.10k.dtseries.nii \
  -var 'data' $D/func_cleaned.10k.dtseries.nii \
  -var 'mean' $D/tmp.dscalar.nii -select 1 1 -repeat

  rm $D/tmp.dscalar.nii

# -----------------------
# REGISTER FUNC -> REFERENCE
# -----------------------
elif [[ $step_name == 'register_standard' ]] ; then
    # Yerkes
    echo 'concat warps'
    convertwarp -o ${D}/transforms/epi2Yerkes \
    --premat=$D/transforms/epi2T1w.mat \
    --warp1=$TD/../MNINonLinear/xfms/acpc_dc2standard \
    -r /vols/Scratch/neichert/NHPPipelines/global/templates/MacaqueYerkes19_T1w_0.5mm_brain_dedrift \

    invwarp -o ${D}/transforms/Yerkes2epi -w ${D}/transforms/epi2Yerkes -r ${D}/raw_restore_mean

    # applywarp -i ${TD}/T1w_acpc_brain_mask \
    # -r /vols/Scratch/neichert/NHPPipelines/global/templates/MacaqueYerkes19_T1w_1.0mm_brain_dedrift \
    # -o $TD/../MNINonLinear/test \
    # -w $TD/../MNINonLinear/xfms/acpc_dc2standard --interp=nn

    echo 'run test'
    applywarp -i ${D}/raw_restore_mean \
    -r /vols/Scratch/neichert/NHPPipelines/global/templates/MacaqueYerkes19_T1w_1.0mm_brain_dedrift \
    -o $TD/../MNINonLinear/test \
    -w ${D}/transforms/epi2Yerkes --interp=nn

elif [[ $step_name == 'apply_standard' ]] ; then
    echo 'warp func to std'
    #mkdir $TD/../MNINonLinear/Results/${task}_func_cleaned
    echo "do $TD/../MNINonLinear/Results/${task}_func_cleaned/${task}_func_cleaned ..."
    applywarp -r /vols/Scratch/neichert/NHPPipelines/global/templates/MacaqueYerkes19_T1w_1.25mm_brain_dedrift \
        -i $D/func_cleaned \
        -o $TD/../MNINonLinear/Results/${task}_func_cleaned/${task}_func_cleaned \
        -w ${D}/transforms/epi2Yerkes \
        --interp=trilinear

        # -i $D/feat.feat/filtered_func_data_cleaned
    echo 'done'

#### ---------------
#  group melodic (redundant, don't use)
#### ---------------
elif [[ $step_name == 'smooth_s' ]] ; then
    wb_command -cifti-smoothing  $SD/${subj}/MNINonLinear/Results/${task}_func_cleaned/${task}_func_cleaned_Atlas.dtseries.nii \
    1 0 COLUMN \
    $SD/${subj}/MNINonLinear/Results/${task}_func_cleaned/${task}_func_cleaned_Atlas_S.dtseries.nii \
    -left-surface $SD/${subj}/MNINonLinear/fsaverage_LR10k/${subj}.L.midthickness.10k_fs_LR.surf.gii \
    -right-surface $SD/${subj}/MNINonLinear/fsaverage_LR10k/${subj}.R.midthickness.10k_fs_LR.surf.gii
    echo 'done'
    echo $SD/${subj}/MNINonLinear/Results/${task}_func_cleaned/${task}_func_cleaned_Atlas_S.dtseries.nii

elif [[ $step_name == 'group_melodic' ]] ; then
    echo 'prepare input files'
    > $SD/group/rest/input_files_DR.txt
    for subj in $subj_list; do
        for p in FH HF; do
            echo $subj $p
            wb_command -cifti-convert -to-nifti \
            $SD/${subj}/MNINonLinear/Results/${p}_func_cleaned/${p}_func_cleaned_Atlas.dtseries.nii \
            $SD/${subj}/MNINonLinear/Results/${p}_func_cleaned/${p}_func_cleaned_Atlas.nii
            echo $SD/${subj}/MNINonLinear/Results/${p}_func_cleaned/${p}_func_cleaned_Atlas.nii >> $SD/group/rest/input_files_DR.txt
        done
    done

    echo 'run melodic'
    melodic -i $SD/group/rest/input_files_DR.txt \
    -o $SD/group/rest/groupICA -d 8 --nobet --nomask

    echo 'do conversion'
    wb_command -cifti-convert -from-nifti \
      /vols/Data/sj/Nicole/site-ucdavis/derivatives/group/rest/groupICA/melodic_IC.nii.gz \
      /vols/Data/sj/Nicole/site-ucdavis/derivatives/sub-032139/MNINonLinear/Results/HF_func_cleaned/HF_func_cleaned_Atlas.dtseries.nii \
      $SD/group/rest/groupICA/melodic_IC.dtseries.nii \
      -reset-timepoints 1 1

fi
